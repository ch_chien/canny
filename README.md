# First-Order Edge Detection (Canny Edge Detection) in C++

Test in Ubuntu 20.04 with g++ version 10.2. 
No dependencies are required.

## 1. Run the code
Under the repo directory, simply do
```bash
make canny
```
Then, run the execution with input arguments
```bash
./canny.out <name_of_input_image>
```
So far only `.pgm` image file is accepted. One sample image is provided under folder `./input_images/`, so you can run the code using:
```bash
./canny.out ./input_images/2018.pgm
```

## 2. Display edges
During running, lists of edges are written in text files under `./output_Data/`. You can use a matlab file `./draw_edges_by_matlab/draw_edges_from_list.m` to plot the edges of the input image.

## 3. Basic Flow of the First-Order Edge Detector

STEP 1: Compute first-order Gaussian filter and convolve the image with the filter

STEP 2: Do non-maximum suppression (NMS) and use double-thresholding to find strong/weak edge points

STEP 3: Connect edges via edge hysteresis
