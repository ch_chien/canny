#ifndef CPU_CANNY_HPP
#define CPU_CANNY_HPP

#include <cmath>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string.h>
#include <vector>

#include "indices.hpp"
#include <omp.h>


template<typename T>
class CannyCPU {
    int img_height;
    int img_width;
    int kernel_sz;
    int gauss_sigma;
    T highThr;
    T lowThr;

    T *img;
    T *grad_mag;
	T *Ix, *Iy;

    // -- update on April 25th --
    T *subpix_pos_x_map;         // -- store x of subpixel location --
    T *subpix_pos_y_map;         // -- store y of subpixel location --
    T *subpix_orientation_map;   // -- store orientation of the edge point --
    T *subpix_grad_mag_map;      // -- store gradient magnitude of the edge points --

  public:

    int *edge_label_map;           // -- store edge point label (0 = noedge, 1 = strong edge, 2 = weak edge) --
    T *subpix_edge_pts_final;    // -- a list of final edge points with all information --
    int edge_pt_list_idx;
    int num_of_edge_data;

    int num_of_candidate_strong;
    int num_of_candidate_weak;
    int num_of_total_candidates;

    int num_of_final_edge_pts;
    int edge_pt_sz;
    int omp_threads;

    // timing
    double time_conv, time_nms, time_edge_track;

    CannyCPU(int, int, int, int, T, int);
    ~CannyCPU();

    void preprocessing(std::ifstream& scan_infile);
    void convolve_img();
    void non_maximum_suppresion();
    //void edge_hysteresis();
    void edge_hysteresis();

    void extract_subpix_edges_for_drawing();

    // -- openmp versions of NMS and edge hysteresis --
    void non_maximum_suppresion_omp();
    void edge_hysteresis_omp();

    void read_array_from_file(std::string filename, T **rd_data, int first_dim, int second_dim);
    void write_array_to_file(std::string filename, T *wr_data, int first_dim, int second_dim);
};

// ==================================== Constructor ===================================
// Define parameters used by functions in the class and allocate 2d arrays dynamically
// ====================================================================================
template<typename T>
CannyCPU<T>::CannyCPU (int H, int W, int kernel_size, int sigma, T high_thr, int nthreads) {
    img_height = H;
    img_width = W;

    kernel_sz = kernel_size;
    gauss_sigma = sigma;
    highThr = high_thr;
    lowThr = 0.4 * highThr;

    // openmp threads
    omp_threads = nthreads;

    grad_mag       = new T[img_height*img_width];
    img            = new T[img_height*img_width];
    Ix             = new T[img_height*img_width];
    Iy             = new T[img_height*img_width];

    subpix_pos_x_map        = new T[img_height*img_width];
    subpix_pos_y_map        = new T[img_height*img_width];
    subpix_orientation_map  = new T[img_height*img_width];
    subpix_grad_mag_map     = new T[img_height*img_width];
    edge_label_map          = new int[img_height*img_width];

    // -- number of data for each edge: subpix x and y, orientation, grad_mag --
    num_of_edge_data = 4;
    subpix_edge_pts_final       = new T[img_height*img_width*num_of_edge_data];
}

// ========================= preprocessing ==========================
// Initialize 2d arrays
// ==================================================================
template<typename T>
void CannyCPU<T>::preprocessing(std::ifstream& scan_infile) {

    for (int i = 0; i < img_height; i++) {
        for (int j = 0; j < img_width; j++) {
            img(i, j) = (int)scan_infile.get();
            Ix(i, j) = 0;
            Iy(i, j) = 0;

            subpix_pos_x_map(i, j)       = 0;
            subpix_pos_y_map(i, j)       = 0;
            subpix_orientation_map(i, j) = 0;
            subpix_grad_mag_map(i, j)    = 0;
            edge_label_map(i, j)         = NO_EDGE;
        }
    }

    for (int i = 0; i < img_height*img_width; i++) {
        for (int j = 0; j < num_of_edge_data; j++) {
            subpix_edge_pts_final(i, j)  = 0;
        }
    }
}

// ================================== convolve Image ==========================================
// We don't pad the input image here as we assume the padded area are all zeros.
// (1) Start by filling in the mask values using the Gausian 1st derivative.
// (2) Do a scanning convolution on the input img matrix. This gives us the Δy and Δx matrices
// (3) Take the sqrt of the sum of Δy^2 and Δx^2 to find the magnitude.
// ============================================================================================
template<typename T>
void CannyCPU<T>::convolve_img()
{
	const int cent = (kernel_sz-1)/2;
	T maskx[kernel_sz * kernel_sz], masky[kernel_sz * kernel_sz];
	T Gx, Gy, G;
	const T PI = 3.14159265358979323846;

	// -- Use the Gaussian 1st derivative formula to fill in the mask values --
	for (int p = -cent; p <= cent; p++) {
		for (int q = -cent; q <= cent; q++) {
			// -- Gaussian derivative in x --
			Gx = (-(q)*std::exp(-(q*q)/(2*gauss_sigma*gauss_sigma)))/(std::sqrt(2*PI)*gauss_sigma*gauss_sigma*gauss_sigma);
			G = std::exp(-(p*p)/(2*gauss_sigma*gauss_sigma))/(std::sqrt(2*PI)*gauss_sigma);
			maskx(p+cent,q+cent) = Gx * G;


			// -- Gaussian derivative in y --
			Gy = (-(p)*std::exp(-(p*p)/(2*gauss_sigma*gauss_sigma)))/(std::sqrt(2*PI)*gauss_sigma*gauss_sigma*gauss_sigma);
			G = std::exp(-(q*q)/(2*gauss_sigma*gauss_sigma))/(std::sqrt(2*PI)*gauss_sigma);
			masky(p+cent,q+cent) = Gy * G;
		}
	}

	// -- do convolution and compute gradient magnitude --
    omp_set_num_threads(omp_threads);
    double start = omp_get_wtime();
    #pragma omp parallel for schedule(dynamic)
	for (int i = 0; i < img_height; i++) {
		for (int j = 0; j < img_width; j++) {

			// loop over the kernel
			for (int p = -cent; p <= cent; p++) {
				for (int q = -cent; q <= cent; q++) {
					if ((i+p) < 0 || (j+q) < 0 || (i+p) >= img_height || (j+q) >= img_width)
						continue;

					Ix(i,j) += img(i+p, j+q) * maskx(p+cent, q+cent);
					Iy(i,j) += img(i+p, j+q) * masky(p+cent, q+cent);
				}
			}

			// -- compute magnitude --
			grad_mag(i, j) = std::sqrt( Ix(i, j) * Ix(i, j) + Iy(i, j) * Iy(i, j) );
		}
	}
    double test_time = omp_get_wtime() - start;
    //std::cout<<"- Time of image convolution (OpenMP): "<<test_time*1000<<" (ms)"<<std::endl;
    time_conv = test_time;
}

// ======================================== Non-maximal Suppression (NMS) ============================================
// (1) Decide the quadrant the gradient belongs to by looking at the signs and size of gradients in x and y directions
// (2) Points which magnitude are greater than both it's neighbors in the direction of their gradients (slope) are
//     considered as peaks.
// (3) Find the subpixel of the edge point by fitting a parabola. This comes from:
//     R. B. Fisher and D. K. Naidu, “A comparison of algorithms for subpixel peak detection,” in Image Technology,
//     Advances in Image Processing, Multimedia and Machine Vis., Berlin, Germany:Springer, 1996, pp. 385–404.
// ====================================================================================================================
template<typename T>
void CannyCPU<T>::non_maximum_suppresion()
{
    double start = omp_get_wtime();
    T norm_dir_x, norm_dir_y;
    T slope, fp, fm;
    T coeff_A, coeff_B, coeff_C, s, s_star;
    T max_f, subpix_grad_x, subpix_grad_y;
    T candidate_edge_pt_x, candidate_edge_pt_y;
    T subpix_grad_mag;
    int local_num_of_strong = 0, local_num_of_weak = 0;

    for (int j = 2; j < img_width - 2; j++) {
        for (int i = 2; i < img_height - 2; i++) {
            // -- ignore neglectable gradient magnitude --
            if (grad_mag(i, j) <= 2)
                continue;

            // -- ignore invalid gradient direction --
            if ((std::abs(Ix(i, j)) < 1e-6) && (std::abs(Ix(i, j)) < 1e-6))
                continue;

            // -- calculate the unit direction --
            norm_dir_x = Ix(i,j) / grad_mag(i,j);
            norm_dir_y = Iy(i,j) / grad_mag(i,j);

            // -- find corresponding quadrant --
            if ((Ix(i,j) >= 0) && (Iy(i,j) >= 0)) {
                if (Ix(i,j) >= Iy(i,j)) {         // -- 1st quadrant --
                    slope = norm_dir_y / norm_dir_x;
                    fp = grad_mag(i, j+1) * (1-slope) + grad_mag(i+1, j+1) * slope;
                    fm = grad_mag(i, j-1) * (1-slope) + grad_mag(i-1, j-1) * slope;
                }
                else {                              // -- 2nd quadrant --
                    slope = norm_dir_x / norm_dir_y;
                    fp = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j+1) * slope;
                    fm = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j-1) * slope;
                }
            }
            else if ((Ix(i,j) < 0) && (Iy(i,j) >= 0)) {
                if (abs(Ix(i,j)) < Iy(i,j)) {     // -- 3rd quadrant --
                    slope = -norm_dir_x / norm_dir_y;
                    fp = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j-1) * slope;
                    fm = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j+1)  * slope;
                }
                else {                              // -- 4th quadrant --
                    slope = -norm_dir_y / norm_dir_x;
                    fp = grad_mag(i, j-1) * (1-slope) + grad_mag(i+1, j-1) * slope;
                    fm = grad_mag(i, j+1) * (1-slope) + grad_mag(i-1, j+1) * slope;
                }
            }
            else if ((Ix(i,j) < 0) && (Iy(i,j) < 0)) {
                if(abs(Ix(i,j)) >= abs(Iy(i,j))) {            // -- 5th quadrant --
                    slope = norm_dir_y / norm_dir_x;
                    fp = grad_mag(i, j-1) * (1-slope) + grad_mag(i-1, j-1) * slope;
                    fm = grad_mag(i, j+1) * (1-slope) + grad_mag(i+1, j+1) * slope;
                }
                else {                              // -- 6th quadrant --
                    slope = norm_dir_x / norm_dir_y;
                    fp = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j-1) * slope;
                    fm = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j+1) * slope;
                }
            }
            else if ((Ix(i,j) >= 0) && (Iy(i,j) < 0)) {
                if(Ix(i,j) < abs(Iy(i,j))) {      // -- 7th quadrant --
                    slope = -norm_dir_x / norm_dir_y;
                    fp = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j+1) * slope;
                    fm = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j-1) * slope;
                }
                else {                              // -- 8th quadrant --
                    slope = -norm_dir_y / norm_dir_x;
                    fp = grad_mag(i, j+1) * (1-slope) + grad_mag(i-1, j+1) * slope;
                    fm = grad_mag(i, j-1) * (1-slope) + grad_mag(i+1, j-1) * slope;
                }
            }

            // -- fit a parabola to find the edge subpixel location when doing max test --
            s = std::sqrt(1+slope*slope);
            if((grad_mag(i, j) >  fm && grad_mag(i, j) > fp) ||  // -- abs max --
               (grad_mag(i, j) >  fm && grad_mag(i, j) >= fp) || // -- relaxed max --
               (grad_mag(i, j) >= fm && grad_mag(i, j) >  fp)) {

                // -- fit a parabola; define coefficients --
                coeff_A = (fm+fp-2*grad_mag(i, j))/(2*s*s);
                coeff_B = (fp-fm)/(2*s);
                coeff_C = grad_mag(i, j);

                s_star = -coeff_B/(2*coeff_A); // -- location of max --
                max_f = coeff_A*s_star*s_star + coeff_B*s_star + coeff_C; // -- value of max --

                if(abs(s_star) <= std::sqrt(2)) { // -- significant max is within a pixel --

                    // -- subpixel magnitude in x and y --
                    subpix_grad_x = max_f*norm_dir_x;
                    subpix_grad_y = max_f*norm_dir_y;

                    // -- subpixel gradient magnitude --
                    subpix_grad_mag = std::sqrt(subpix_grad_x*subpix_grad_x + subpix_grad_y*subpix_grad_y);

                    // -- double thresholding: decide whether the edge point is a strong or weak point, or should be eliminated --
                    // -- Put strong points in subpix_edge_pts_strong array --
                    // -- Put weak points in subpix_edge_pts_weak array, also put weak point index in a index map (subpix_idx_map) --
                    if (subpix_grad_mag >= highThr) {   // -- strong edge point --

                        // -- store subpixel location in a map --
                        subpix_pos_x_map(i, j) = j + s_star * norm_dir_x;
                        subpix_pos_y_map(i, j) = i + s_star * norm_dir_y;

                        // -- store gradient of subpixel edge in the map --
                        subpix_grad_mag_map(i, j) = subpix_grad_mag;

                        // -- store orientation of the edge in a map --
                        subpix_orientation_map(i, j) = std::atan2(norm_dir_x, -norm_dir_y);

                        // -- encode a edge label (1 = strong) --
                        edge_label_map(i, j) = /*1*/ EDGE_STRONG;

                        // -- count the number of strong edges locally --
                        local_num_of_strong++;
                    }
                    else if ((subpix_grad_mag < highThr) && (subpix_grad_mag > lowThr)) {   // -- weak edge point --

                        // -- store subpixel location in a map --
                        subpix_pos_x_map(i, j) = j + s_star * norm_dir_x;
                        subpix_pos_y_map(i, j) = i + s_star * norm_dir_y;

                        // -- store gradient of subpixel edge in the map --
                        subpix_grad_mag_map(i, j) = subpix_grad_mag;

                        // -- store orientation of the edge in a map --
                        subpix_orientation_map(i, j) = std::atan2(norm_dir_x, -norm_dir_y);

                        // -- encode a edge label (2 = weak) --
                        edge_label_map(i, j) = /*2*/ EDGE_WEAK;

                        // -- count the number of weak edges locally --
                        local_num_of_weak++;
                    }
                    else {
                        continue;
                    }
                }
            }
        }
    }
    double end = omp_get_wtime() - start;
    //std::cout<<"- Time of NMS (Sequential) = "<<end*1000<<" (ms)"<<std::endl;
    time_nms = end;

    // -- number of strong and weak edge points --
    num_of_candidate_strong = local_num_of_strong;
    num_of_candidate_weak = local_num_of_weak;

    // -- number of total edge candidates --
    num_of_total_candidates = local_num_of_strong + local_num_of_weak;

    //std::cout<<"Number of STRONG and WEAK candidate edge points: "<<num_of_candidate_strong<<", "<<num_of_candidate_weak<<std::endl;
}

// ======================================== Edge Hysteresis (Edge Tracking) ============================================
// Edge hysteresis decides which weak points should be turned into strong points, and which should be disappeared. This
// is done by looking at the 3x3 neighbors of a strong points, and if there is a weak point in the neigbor, make that weak
// point to a strong point. After looking at the neighbors of "ALL the strong points", including those who initially are
// strong points and those who turned from weak to strong, if there is no weak points, then edge hysteresis is finished.
// Process:
// (1) Look at each strong edge in the subpix_edge_pts_strong array; if a weak edge appears in its 3x3 neighborhood, put
//     that weak edge to the end of the subpix_edge_pts_strong array.
// (2) End the process when all edges in the subpix_edge_pts_strong array has been visited.
// ======================================================================================================================
template<typename T>
void CannyCPU<T>::edge_hysteresis() {
    bool reiterate_flag = 1;
    int final_edge_counter = 0;

    double start = omp_get_wtime();
    while (reiterate_flag) {
        reiterate_flag = 0;

        // -- loop over the edge_label_map --
        for (int i = 0; i < img_height; i++) {
            for (int j = 0; j < img_width; j++) {
                // -- continue looping if it is neither a strong edge (label 0 or 2) nor a final edge (label 3) --
                if (edge_label_map(i, j) != /*1*/ EDGE_STRONG) {
                    continue;
                }

                // -- loop over the 3x3 neighbors of the center strong edge point --
                for (int p = -1; p <= 1; p++) {
                    for (int q = -1; q <= 1; q++) {

                        // -- if a neighbor is a weak edge, turn the corresponding label from 2 to 1 --
                        if (edge_label_map(i+p, j+q) == /*2*/ EDGE_WEAK) {
                            edge_label_map(i+p, j+q) = /*1*/ EDGE_STRONG;
                            reiterate_flag = 1;
                        }
                    }
                }

                // -- turn the label of the center strong edge as 3 --
                edge_label_map(i, j) = /*3*/ EDGE_FINAL_STRONG;

                // -- accumulate the number of final edge points --
                final_edge_counter++;
            }
        }
    }
    double end = omp_get_wtime() - start;
    //std::cout<<"- Time of Edge Hysteresis (Sequential) = "<<end*1000<<" (ms)"<<std::endl;
    time_edge_track = end;

    num_of_final_edge_pts = final_edge_counter;
    //std::cout<<"Number of final edge points  = "<<num_of_final_edge_pts<<std::endl;
}

template<typename T>
void CannyCPU<T>::extract_subpix_edges_for_drawing() {
    // -- loop over to the edge_label_map to extract final edges and extract corresponding subpixels --
    edge_pt_list_idx = 0;
    for (int i = 0; i < img_height; i++) {
        for (int j = 0; j < img_width; j++) {
            if (edge_label_map(i, j) == /*3*/ EDGE_FINAL_STRONG) {
                // -- store all necessary information of final edges --
                // -- 1) subpixel location x --
                subpix_edge_pts_final(edge_pt_list_idx, 0) = subpix_pos_x_map(i, j);

                // -- 2) subpixel location y --
                subpix_edge_pts_final(edge_pt_list_idx, 1) = subpix_pos_y_map(i, j);

                // -- 3) orientation --
                subpix_edge_pts_final(edge_pt_list_idx, 2) = subpix_orientation_map(i, j);

                // -- 4) subpixel gradient magnitude --
                subpix_edge_pts_final(edge_pt_list_idx, 3) = subpix_grad_mag_map(i, j);

                // -- 5) add up the edge point list index --
                edge_pt_list_idx++;
            }
            else {
                continue;
            }
        }
    }
    //std::cout<<"MATLAB fecth data number = "<<edge_pt_list_idx<<std::endl;

    printf(" ## Convolution time = %8.4f ms\n", time_conv * 1000.);
    printf(" ## NMS         time = %8.4f ms\n", time_nms  * 1000.);
    printf(" ## Edge-track  time = %8.4f ms\n", time_edge_track * 1000.);
    printf("\n");
    printf(" Strong candidates   = %6d\n", num_of_candidate_strong);
    printf(" Weak   candidates   = %6d\n", num_of_candidate_weak);
    printf(" Final edge points   = %6d\n", num_of_final_edge_pts);

    // -- write data back so that MATLAB can fetch subpixel data and draw final edge map --
    write_array_to_file("data_final_output.txt", subpix_edge_pts_final, edge_pt_list_idx, num_of_edge_data);
}

// ===================================== Write data to file for debugging =======================================
// Writes a 2d dybamically allocated array to a text file for debugging
// ==============================================================================================================
template<typename T>
void CannyCPU<T>::write_array_to_file(std::string filename, T *wr_data, int first_dim, int second_dim)
{
#define wr_data(i, j) wr_data[(i) * second_dim + (j)]

    std::cout<<"writing data to a file "<<filename<<" ..."<<std::endl;
    std::string out_file_name = "./test_files/";
    out_file_name.append(filename);
	std::ofstream out_file;
    out_file.open(out_file_name);
    if ( !out_file.is_open() )
      std::cout<<"write data file cannot be opened!"<<std::endl;

	for (int i = 0; i < first_dim; i++) {
		for (int j = 0; j < second_dim; j++) {
			out_file << wr_data(i, j) <<"\t";
		}
		out_file << "\n";
	}

    out_file.close();
#undef wr_data
}

// ===================================== Write data to file for debugging =======================================
// Reads data for debugging
// ==============================================================================================================
template<typename T>
void CannyCPU<T>::read_array_from_file(std::string filename, T **rd_data, int first_dim, int second_dim) {
    std::cout<<"reading data from a file ..."<<std::endl;
    std::string in_file_name = "./test_files/";
    in_file_name.append(filename);
    std::fstream in_file;
    T data;
    int idx_x = 0, idx_y = 0;

    in_file.open(in_file_name, std::ios_base::in);
    if (!in_file) {
        std::cerr << "input read file not existed!\n";
    }
    else {
        while (in_file >> data) {
            rd_data[idx_y][idx_x] = data;
            idx_x++;
            if (idx_x == second_dim) {
                idx_x = 0;
                idx_y++;
            }
        }
    }
}

// ==========================================================================================================
// ============================== OPENMP VERSION OF NMS AND EDGE HYSTERESIS =================================
// ==========================================================================================================

// ================================== OPENMP VERSION OF NMS =====================================
template<typename T>
void CannyCPU<T>::non_maximum_suppresion_omp()
{
    int local_num_of_strong = 0, local_num_of_weak = 0;

    omp_set_num_threads(omp_threads);
    double start = omp_get_wtime();
    #pragma omp parallel
    {
        // -- local varaibles for openmp threads --
        T norm_dir_x, norm_dir_y;
        T slope, fp, fm;
        T coeff_A, coeff_B, coeff_C, s, s_star;
        T max_f, subpix_grad_x, subpix_grad_y;
        T candidate_edge_pt_x, candidate_edge_pt_y;
        T subpix_grad_mag;

        #pragma omp for schedule(dynamic) reduction(+: local_num_of_strong, local_num_of_weak)
        for (int j = 2; j < img_width - 2; j++) {
            for (int i = 2; i < img_height - 2; i++) {


                // -- ignore neglectable gradient magnitude --
                if (grad_mag(i, j) <= 2)
                    continue;

                // -- ignore invalid gradient direction --
                if ((std::abs(Ix(i, j)) < 1e-6) && (std::abs(Ix(i, j)) < 1e-6))
                    continue;

                // -- calculate the unit direction --
                norm_dir_x = Ix(i,j) / grad_mag(i,j);
                norm_dir_y = Iy(i,j) / grad_mag(i,j);

                // -- find corresponding quadrant --
                if ((Ix(i,j) >= 0) && (Iy(i,j) >= 0)) {
                    if (Ix(i,j) >= Iy(i,j)) {         // -- 1st quadrant --
                        slope = norm_dir_y / norm_dir_x;
                        fp = grad_mag(i, j+1) * (1-slope) + grad_mag(i+1, j+1) * slope;
                        fm = grad_mag(i, j-1) * (1-slope) + grad_mag(i-1, j-1) * slope;
                    }
                    else {                              // -- 2nd quadrant --
                        slope = norm_dir_x / norm_dir_y;
                        fp = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j+1) * slope;
                        fm = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j-1) * slope;
                    }
                }
                else if ((Ix(i,j) < 0) && (Iy(i,j) >= 0)) {
                    if (abs(Ix(i,j)) < Iy(i,j)) {     // -- 3rd quadrant --
                        slope = -norm_dir_x / norm_dir_y;
                        fp = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j-1) * slope;
                        fm = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j+1)  * slope;
                    }
                    else {                              // -- 4th quadrant --
                        slope = -norm_dir_y / norm_dir_x;
                        fp = grad_mag(i, j-1) * (1-slope) + grad_mag(i+1, j-1) * slope;
                        fm = grad_mag(i, j+1) * (1-slope) + grad_mag(i-1, j+1) * slope;
                    }
                }
                else if ((Ix(i,j) < 0) && (Iy(i,j) < 0)) {
                    if(abs(Ix(i,j)) >= abs(Iy(i,j))) {            // -- 5th quadrant --
                        slope = norm_dir_y / norm_dir_x;
                        fp = grad_mag(i, j-1) * (1-slope) + grad_mag(i-1, j-1) * slope;
                        fm = grad_mag(i, j+1) * (1-slope) + grad_mag(i+1, j+1) * slope;
                    }
                    else {                              // -- 6th quadrant --
                        slope = norm_dir_x / norm_dir_y;
                        fp = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j-1) * slope;
                        fm = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j+1) * slope;
                    }
                }
                else if ((Ix(i,j) >= 0) && (Iy(i,j) < 0)) {
                    if(Ix(i,j) < abs(Iy(i,j))) {      // -- 7th quadrant --
                        slope = -norm_dir_x / norm_dir_y;
                        fp = grad_mag(i-1, j) * (1-slope) + grad_mag(i-1, j+1) * slope;
                        fm = grad_mag(i+1, j) * (1-slope) + grad_mag(i+1, j-1) * slope;
                    }
                    else {                              // -- 8th quadrant --
                        slope = -norm_dir_y / norm_dir_x;
                        fp = grad_mag(i, j+1) * (1-slope) + grad_mag(i-1, j+1) * slope;
                        fm = grad_mag(i, j-1) * (1-slope) + grad_mag(i+1, j-1) * slope;
                    }
                }

                // -- fit a parabola to find the edge subpixel location when doing max test --
                s = std::sqrt(1+slope*slope);
                if((grad_mag(i, j) >  fm && grad_mag(i, j) > fp) ||  // -- abs max --
                (grad_mag(i, j) >  fm && grad_mag(i, j) >= fp) || // -- relaxed max --
                (grad_mag(i, j) >= fm && grad_mag(i, j) >  fp)) {

                    // -- fit a parabola; define coefficients --
                    coeff_A = (fm+fp-2*grad_mag(i, j))/(2*s*s);
                    coeff_B = (fp-fm)/(2*s);
                    coeff_C = grad_mag(i, j);

                    s_star = -coeff_B/(2*coeff_A); // -- location of max --
                    max_f = coeff_A*s_star*s_star + coeff_B*s_star + coeff_C; // -- value of max --

                    if(abs(s_star) <= std::sqrt(2)) { // -- significant max is within a pixel --

                        // -- subpixel magnitude in x and y --
                        subpix_grad_x = max_f*norm_dir_x;
                        subpix_grad_y = max_f*norm_dir_y;

                        // -- subpixel gradient magnitude --
                        subpix_grad_mag = std::sqrt(subpix_grad_x*subpix_grad_x + subpix_grad_y*subpix_grad_y);

                        // -- double thresholding: decide whether the edge point is a strong or weak point, or should be eliminated --
                        // -- Put strong points in subpix_edge_pts_strong array --
                        // -- Put weak points in subpix_edge_pts_weak array, also put weak point index in a index map (subpix_idx_map) --
                        if (subpix_grad_mag >= highThr) {   // -- strong edge point --

                            // -- store subpixel location in a map --
                            subpix_pos_x_map(i, j) = j + s_star * norm_dir_x;
                            subpix_pos_y_map(i, j) = i + s_star * norm_dir_y;

                            // -- store gradient of subpixel edge in the map --
                            subpix_grad_mag_map(i, j) = subpix_grad_mag;

                            // -- store orientation of the edge in a map --
                            subpix_orientation_map(i, j) = std::atan2(norm_dir_x, -norm_dir_y);

                            // -- encode a edge label (1 = strong) --
                            edge_label_map(i, j) = /*1*/ EDGE_STRONG;

                            // -- count the number of strong edges locally --
                            local_num_of_strong++;
                        }
                        else if ((subpix_grad_mag < highThr) && (subpix_grad_mag > lowThr)) {   // -- weak edge point --

                            // -- store subpixel location in a map --
                            subpix_pos_x_map(i, j) = j + s_star * norm_dir_x;
                            subpix_pos_y_map(i, j) = i + s_star * norm_dir_y;

                            // -- store gradient of subpixel edge in the map --
                            subpix_grad_mag_map(i, j) = subpix_grad_mag;

                            // -- store orientation of the edge in a map --
                            subpix_orientation_map(i, j) = std::atan2(norm_dir_x, -norm_dir_y);

                            // -- encode a edge label (2 = weak) --
                            edge_label_map(i, j) = /*2*/ EDGE_WEAK;

                            // -- count the number of weak edges locally --
                            local_num_of_weak++;
                        }
                        else {
                            continue;
                        }
                    }
                }
            }
        }
    }
    double end = omp_get_wtime() - start;
    //std::cout<<"- Time of NMS (OpenMP) = "<<end*1000<<" (ms)"<<std::endl;
    time_nms = end;
    // -- number of strong and weak edge points --
    num_of_candidate_strong = local_num_of_strong;
    num_of_candidate_weak = local_num_of_weak;

    // -- number of total edge candidates --
    num_of_total_candidates = local_num_of_strong + local_num_of_weak;

    //std::cout<<"Number of STRONG and WEAK candidate edge points: "<<num_of_candidate_strong<<", "<<num_of_candidate_weak<<std::endl;
}

template<typename T>
void CannyCPU<T>::edge_hysteresis_omp() {

    bool reiterate_flag = 1;
    int final_edge_counter = 0;
    T end_time = 0;

    omp_set_num_threads(omp_threads);
    while (reiterate_flag) {
        reiterate_flag = 0;

        // -- loop over the edge_label_map --
        double start = omp_get_wtime();
        #pragma omp parallel for schedule(dynamic) reduction(+: final_edge_counter) reduction(|: reiterate_flag)
        for (int i = 0; i < img_height; i++) {
            for (int j = 0; j < img_width; j++) {
                // -- continue looping if it is neither a strong edge (label 0 or 2) nor a final edge (label 3) --
                if (edge_label_map(i, j) != /*1*/ EDGE_STRONG) {
                    continue;
                }

                // -- loop over the 3x3 neighbors of the center strong edge point --
                for (int p = -1; p <= 1; p++) {
                    for (int q = -1; q <= 1; q++) {

                        // -- if a neighbor is a weak edge, turn the corresponding label from 2 to 1 --
                        if (edge_label_map(i+p, j+q) == /*2*/ EDGE_WEAK) {
                            edge_label_map(i+p, j+q) = /*1*/ EDGE_STRONG;
                            reiterate_flag = 1;
                        }
                    }
                }

                // -- turn the label of the center strong edge as 3 --
                edge_label_map(i, j) = /*3*/ EDGE_FINAL_STRONG;

                // -- accumulate the number of final edge points --
                final_edge_counter++;
            }
        }
        end_time += omp_get_wtime() - start;
    }

    //std::cout<<"- Time of Edge Hysteresis (OpenMP) = "<<end_time*1000<<" (ms)"<<std::endl;
    time_edge_track = end_time;

    num_of_final_edge_pts = final_edge_counter;
    //std::cout<<"Number of final edge points  = "<<num_of_final_edge_pts<<std::endl;
}

// ===================================== Destructor =======================================
// Free all the 2d dynamic arrays allocated in the constructor
// ========================================================================================
template<typename T>
CannyCPU<T>::~CannyCPU () {
    // free memory
    delete[] img;
    delete[] grad_mag;
    delete[] Ix;
    delete[] Iy;

    delete[] subpix_pos_x_map;
    delete[] subpix_pos_y_map;
    delete[] subpix_orientation_map;
    delete[] subpix_grad_mag_map;
    delete[] edge_label_map;

    delete[] subpix_edge_pts_final;
}

#endif    // CPU_CANNY_HPP