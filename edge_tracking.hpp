#ifndef EDGE_TRACKING_HPP
#define EDGE_TRACKING_HPP

#include <cmath>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string.h>
#include <vector>

#include "indices.hpp"
#include <omp.h>


class EdgeTrackingCPU {
    int img_height;
    int img_width;
    
    int *edge_map_nms;
    int *contour_map;
    int *junction_endpt_map;
    int *contour_graph_x;
    int *contour_graph_y;

    // -- used for drawing in matlab --
    double *subpix_pos_x_map;
    double *subpix_pos_y_map;
    double *subpix_contour_list;

  public:

    int num_of_final_edge_pts;
    int omp_threads;

    // -- shared index/label across all class member functions --
    int next_edge_x, next_edge_y;
    int c_label;
    int contour_node_idx;

    // timing
    double time_edge_hysteresis_and_junction_endpt_detection;
    double time_contour_extraction;

    EdgeTrackingCPU(int, int, int);
    ~EdgeTrackingCPU();

    void preprocessing();
    void edge_tracking();

    void edge_hysteresis_and_junction_endpt_detection();
    void find_next_edge(int center_x, int center_y);
    void find_next_edge_in_loop(int center_x, int center_y);
    void track_from_endpt();
    void track_from_remains();

    void contour_drawing_for_matlab();
    
    // -- read & write --
    void read_int_array_from_file(std::string filename, int *rd_data, int first_dim, int second_dim);
    void read_double_array_from_file(std::string filename, double *rd_data, int first_dim, int second_dim);
    void write_int_array_to_file(std::string filename, int *wr_data, int first_dim, int second_dim);
    void write_double_array_to_file(std::string filename, double *wr_data, int first_dim, int second_dim);
};

// ==================================== Constructor ===================================
// Define parameters used by functions in the class and allocate 2d arrays dynamically
// ====================================================================================
EdgeTrackingCPU::EdgeTrackingCPU (int H, int W, int cpu_nthreads) {
    img_height = H;
    img_width = W;

    omp_threads = cpu_nthreads;

    edge_map_nms             = new int[img_height*img_width];
    contour_map              = new int[img_height*img_width];
    junction_endpt_map             = new int[img_height*img_width];

    subpix_pos_x_map         = new double[img_height*img_width];
    subpix_pos_y_map         = new double[img_height*img_width];
    subpix_contour_list      = new double[img_height*img_width*3];

    contour_graph_x          = new int[5000*img_height*3];
    contour_graph_y          = new int[5000*img_height*3];
}

// ========================= preprocessing ==========================
// Initialize 2d arrays
// ==================================================================
void EdgeTrackingCPU::preprocessing() {

    // -- initialize --
    c_label = 1;
    contour_node_idx = 0;
    next_edge_x = 0;
    next_edge_y = 0;

    // -- read edge map after NMS --
    std::string example_edge_map_filename;
    if (img_height == 32 && img_width == 32) {
        example_edge_map_filename = "edge_map_doubleThr.txt";
    }
    else {
        // -- the 2018.pgm image --

        example_edge_map_filename = "edge_map_nms.txt";
        // -- read subpix x and y maps --
        std::string subpix_pos_x_map_filename = "subpix_pos_x_map.txt";
        std::string subpix_pos_y_map_filename = "subpix_pos_y_map.txt";
        read_double_array_from_file(subpix_pos_x_map_filename, subpix_pos_x_map, img_height, img_width);
        read_double_array_from_file(subpix_pos_y_map_filename, subpix_pos_y_map, img_height, img_width);
    }

    read_int_array_from_file(example_edge_map_filename, edge_map_nms, img_height, img_width);

    // -- initialize arrays to zero --
    for (int i = 0; i < img_height; i++) {
        for (int j = 0; j < img_width; j++) {
            contour_map(i, j) = 0;
            junction_endpt_map(i, j) = 0;
        }
    }

    // -- initialize arrays to (-1) --
    for (int i = 0; i < 5000; i++) {
        for (int j = 0; j < img_height*3; j++) {
            contour_graph_x(i, j) = -1;
            contour_graph_y(i, j) = -1;
        }
    }
}

// ========================= edge_tracking ==========================
// proceed all steps to complete edge tracking task
// ==================================================================
void EdgeTrackingCPU::edge_tracking() {

    // -- do edge hysteresis while finding junctions and end points at the same time --
    edge_hysteresis_and_junction_endpt_detection();

    // -- start tracking edges from all end-points --
    track_from_endpt();

    // -- start tracking edges from any edge left to be explored --
    track_from_remains();

}

// ========================= edge_hysteresis_and_junction_endpt_detection  ==========================
// do edge hysteresis and detect junction as well as end-points at the same time
// ==================================================================================================
void EdgeTrackingCPU::edge_hysteresis_and_junction_endpt_detection() {
    bool reiterate_flag = 1;
    int final_edge_counter = 0;
    double end_time = 0;

    // -- count the number of neighbors (strong / weak turn to strong) for edge tracking --
    int num_neighbor_edges = 0;
    int neighbor_x_offset, neighbor_y_offset;
    int N = 0;
    int M = 0;
    int dist = 0;
    int dist_neighbor1, dist_neighbor2, dist_neighbor_pairs;
    int neighbor_edges_offset_x[8] = {0};
    int neighbor_edges_offset_y[8] = {0};

    omp_set_num_threads(1);
    while (reiterate_flag) {
        reiterate_flag = 0;

        // -- loop over the edge_map_nms --
        double start = omp_get_wtime();
        for (int i = 0; i < img_height; i++) {
            for (int j = 0; j < img_width; j++) {
                // -- continue looping if it is neither a strong edge (label 0 or 2) nor a final edge (label 3) --
                if (edge_map_nms(i, j) != /*1*/ EDGE_STRONG) {
                    continue;
                }

                // -- reset --
                num_neighbor_edges = 0;
                N = 0;
                M = 0;
                neighbor_edges_offset_x[8] = {0};
                neighbor_edges_offset_y[8] = {0};

                // -- loop over the 3x3 neighbors of the center strong edge point --
                for (int p = -1; p <= 1; p++) {
                    for (int q = -1; q <= 1; q++) {

                        // -- if a neighbor is a weak edge, turn the corresponding label from 2 to 1 --
                        if (edge_map_nms(i+p, j+q) == /*0*/ EDGE_WEAK) {
                            edge_map_nms(i+p, j+q) = /*1*/ EDGE_STRONG;
                            reiterate_flag = 1;

                            // -- record neighbor edges --
                            if (p != 0 || q != 0) {
                                neighbor_edges_offset_x[num_neighbor_edges] = p;
                                neighbor_edges_offset_y[num_neighbor_edges] = q;
                                num_neighbor_edges++;
                            }
                        }
                        else if (edge_map_nms(i+p, j+q) == /*1*/ EDGE_STRONG || edge_map_nms(i+p, j+q) == /*10*/ EDGE_FINAL_STRONG) {
                            
                            // -- record neighbor edges --
                            if (p != 0 || q != 0) {
                                neighbor_edges_offset_x[num_neighbor_edges] = p;
                                neighbor_edges_offset_y[num_neighbor_edges] = q;
                                num_neighbor_edges++;
                            }
                        }
                    }
                }

                // -- decide whether the edge is a junction, end-point, or neither of them --
                #if DETECT_JUNCTION_AND_ENDPOINT
                if (num_neighbor_edges >= 3) {
                    // -- DETECT JUNCTIONS --
                    // -- if num_neighbor_edges >= 3, let M = number of neighbor pairs, and N = number of neighbor pairs of which the Manhattan distance is >= 2 pixels --
                    // -- then we do case by case: (1) if num_neighbor_edges = 3, then an edge is a junction if M == N                                                  --
                    // --                          (2) if num_neighbor_edges = 4, then an edge is a junction if M-1 <= N                                                --
                    // --                          (3) if num_neighbor_edges = 5, then an edge is a junction if M-2 <= N                                                --
                    // --                          (4) if num_neighbor_edges > 5, this is too rare to consider. Normally it does not exist.                             --

                    // -- compute M --
                    for (int k = 1; k < num_neighbor_edges; k++) {
                        M += k;
                    }
                    // -- compute Manhattan distance between neighbor pairs to calculate N --
                    for (int m = 0; m < num_neighbor_edges; m++) {
                        neighbor_x_offset = neighbor_edges_offset_x[m];
                        neighbor_y_offset = neighbor_edges_offset_y[m];
                        for (int n = m+1; n < num_neighbor_edges; n++) {
                            dist = std::abs(neighbor_x_offset - neighbor_edges_offset_x[n]) + std::abs(neighbor_y_offset - neighbor_edges_offset_y[n]);
                            if (dist < 2)
                                continue;
                            else
                                N++;
                        }
                    }

                    // -- decide whether the edge is a junction or not --
                    if (num_neighbor_edges == 3) {
                        junction_endpt_map(i, j) = (N == M) ? EDGE_JUNCTION : 0;
                    }
                    else if (num_neighbor_edges == 4) {
                        junction_endpt_map(i, j) = (N >= M-1) ? EDGE_JUNCTION : 0;
                    }
                    else if (num_neighbor_edges == 5) {
                        junction_endpt_map(i, j) = (N >= M-2) ? EDGE_JUNCTION : 0;
                    }
                }
                else {
                    // -- DETECT END-POINTS --
                    // -- if num_neighbor_edges < 3, then we do case by case:                                                                                   --
                    // -- (1) if num_neighbor_edges = 1, then an edge is an end-point                                                                           --
                    // -- (2) if num_neighbor_edges = 2, let dist_neighbor1 and dist_neighbor2 be absolute sums of off-sets of the two neighbor edges,          --
                    // --     and dist_neighbor_pairs be the Manhattan distance between the two neighbor edges, then an edge is an end-point if                 --
                    // --     (dist_neighbor_pairs == 1) && (dist_neighbor1 + dist_neighbor2 == 3)                                                              --
                    
                    if (num_neighbor_edges == 1)
                        junction_endpt_map(i, j) = EDGE_END;
                    else if (num_neighbor_edges == 2) {
                        dist_neighbor1 = std::abs(neighbor_edges_offset_x[0]) + std::abs(neighbor_edges_offset_y[0]);
                        dist_neighbor2 = std::abs(neighbor_edges_offset_x[1]) + std::abs(neighbor_edges_offset_y[1]);
                        dist_neighbor_pairs = std::abs(neighbor_edges_offset_x[0] - neighbor_edges_offset_x[1]) + std::abs(neighbor_edges_offset_y[0] - neighbor_edges_offset_y[1]);
                        if (dist_neighbor_pairs == 1 && (dist_neighbor1 + dist_neighbor2) == 3)
                            junction_endpt_map(i, j) = EDGE_END;
                    }
                }
                #endif

                // -- turn the label of the center strong edge as 3 --
                edge_map_nms(i, j) = /*3*/ EDGE_FINAL_STRONG;

                // -- accumulate the number of final edge points --
                final_edge_counter++;
            }
        }
        end_time += omp_get_wtime() - start;
    }
    time_edge_hysteresis_and_junction_endpt_detection = end_time;
    //std::cout<<"- Time of Edge Hysteresis & Junction Detection (Sequential) = "<<time_edge_hysteresis_and_junction_endpt_detection*1000<<" (ms)"<<std::endl;

    //num_of_final_edge_pts = final_edge_counter;
    //std::cout<<"Number of final edge points  = "<<num_of_final_edge_pts<<std::endl;

    write_int_array_to_file("edge_map_hyst_and_junc_endpt_detection.txt", edge_map_nms, img_height, img_width);

    #if DETECT_JUNCTION_AND_ENDPOINT
    write_int_array_to_file("junction_endpt_map.txt", junction_endpt_map, img_height, img_width);
    #endif
}

// ========================= find_next_edge  ==========================
// find next edge to track, used in the track_from_endpt() function
// ====================================================================
void EdgeTrackingCPU::find_next_edge(int center_x, int center_y) {
    int neighbor_offset_x[8] = {0}, neighbor_offset_y[8] = {0};
    int junction_offset_x = 0, junction_offset_y = 0;
    int neighbor_idx = 0, junction_idx = 0;
    int dist = 0, dist_junction = 0, dist_neighbor = 0;
    int min_dist = 10;
    bool junction_exist = 0;
    bool keep_tracking = 1;

    // -- 1) loop over 3x3 neighbors and store offsets of neighbor edges --
    for (int p = -1; p <= 1; p++) {
        for (int q = -1; q <= 1; q++) {

            // -- exclude the center edge, check whether there is a junction neighbor, and store non-junction neghbor edges --
            if (p != 0 || q != 0) {
                if (junction_endpt_map(center_x+p, center_y+q) == EDGE_JUNCTION) {
                    junction_exist = 1;
                    junction_offset_x = p;
                    junction_offset_y = q;
                }
                else if (edge_map_nms(center_x+p, center_y+q) == EDGE_FINAL_STRONG && contour_map(center_x+p, center_y+q) == 0) {
                    neighbor_offset_x[neighbor_idx] = p;
                    neighbor_offset_y[neighbor_idx] = q;
                    neighbor_idx++;
                }
            }
        }
    }

    // -- 2) find the next tracking neighbor edge --
    if (junction_exist) {
        // -- if there is a junction neighbor, then
        // -- (1) if neighbor_idx > 0, decide whether the non-junction neighbor edge should be the next edge to track by    --
        // --     comparing the absolute sum of offsets of the junction and the neighbor edge                               --
        // -- (2) otherwise, junction edge is the next edge to track                                                        --
        if (neighbor_idx > 0) {
            dist_junction = std::abs(junction_offset_x) + std::abs(junction_offset_y);
            dist_neighbor = std::abs(neighbor_offset_x[0]) + std::abs(neighbor_offset_y[0]);
            next_edge_x = (dist_neighbor <= dist_junction) ? (center_x + neighbor_offset_x[0]) : (center_x + junction_offset_x);
            next_edge_y = (dist_neighbor <= dist_junction) ? (center_y + neighbor_offset_y[0]) : (center_y + junction_offset_y);
        }
        else {
            next_edge_x = center_x + junction_offset_x;
            next_edge_y = center_y + junction_offset_y;
        }
    }
    else {
        // -- if there is no junction neighbor, then                                                                                        --
        // -- (1) if there are no neighbor edge to track, make (next_edge_x, next_edge_y) = (-1, -1)                                        --
        // -- (2) if there are only 1 neighbor edge to track, make it as the next edge to track                                             --
        // -- (3) if there are 2 neighbor edges, choose which edge as the next edge to track by comparing the absolute sum of their offsets --
        if (neighbor_idx == 0) {
            next_edge_x = -1;
            next_edge_y = -1;
        }
        else if (neighbor_idx == 1) {
            next_edge_x = center_x + neighbor_offset_x[0];
            next_edge_y = center_y + neighbor_offset_y[0];
        }
        else if (neighbor_idx > 1) {
            for (int k = 0; k < neighbor_idx; k++) {
                dist = std::abs(neighbor_offset_x[k]) + std::abs(neighbor_offset_y[k]);
                if (dist < min_dist) {
                    next_edge_x = center_x + neighbor_offset_x[k];
                    next_edge_y = center_y + neighbor_offset_y[k];
                    min_dist = dist;
                }
            }
        }
    }
}

// ========================= find_next_edge_in_loop  ==========================
// find next edge to track, used in the track_from_remains() function
// ============================================================================
void EdgeTrackingCPU::find_next_edge_in_loop(int center_x, int center_y) {
    int neighbor_offset_x[8] = {0}, neighbor_offset_y[8] = {0};
    int junction_offset_x = 0, junction_offset_y = 0;
    int neighbor_idx = 0, junction_idx = 0;
    int dist = 0, dist_junction = 0, dist_neighbor = 0;
    int min_dist = 10;
    bool junction_exist = 0;
    bool keep_tracking = 1;

    // -- 1) loop over 3x3 neighbors and store offsets of neighbor edges --
    for (int p = -1; p <= 1; p++) {
        for (int q = -1; q <= 1; q++) {

            // -- exclude the center edge, check whether there is a junction neighbor, and store non-junction neghbor edges --
            if (p != 0 || q != 0) {
                if (edge_map_nms(center_x+p, center_y+q) == EDGE_FINAL_STRONG && contour_map(center_x+p, center_y+q) == 0) {
                    neighbor_offset_x[neighbor_idx] = p;
                    neighbor_offset_y[neighbor_idx] = q;
                    neighbor_idx++;
                }
            }
        }
    }

    // -- 2) find the next tracking neighbor edge
    // -- if there is no junction neighbor, then                                                                                        --
    // -- (1) if there are no neighbor edge to track, make (next_edge_x, next_edge_y) = (-1, -1)                                        --
    // -- (2) if there are only 1 neighbor edge to track, make it as the next edge to track                                             --
    // -- (3) if there are 2 neighbor edges, choose which edge as the next edge to track by comparing the absolute sum of their offsets --
    if (neighbor_idx == 0) {
        next_edge_x = -1;
        next_edge_y = -1;
    }
    else if (neighbor_idx == 1) {
        next_edge_x = center_x + neighbor_offset_x[0];
        next_edge_y = center_y + neighbor_offset_y[0];
    }
    else if (neighbor_idx > 1) {
        for (int k = 0; k < neighbor_idx; k++) {
            dist = std::abs(neighbor_offset_x[k]) + std::abs(neighbor_offset_y[k]);
            if (dist < min_dist) {
                next_edge_x = center_x + neighbor_offset_x[k];
                next_edge_y = center_y + neighbor_offset_y[k];
                min_dist = dist;
            }
        }
    }

}

// =========================== track_from_endpt ========================
// track edges to form contours, starting from end-point edges
// =====================================================================
void EdgeTrackingCPU::track_from_endpt() {
    c_label = 1;
    int target_x, target_y;
    int contour_seg_idx = 0;
    bool keep_tracking = 1;
    contour_node_idx = 0;

    // -- loop over all images to ensure that every end-point edge is explored --
    for (int i = 0; i < img_height; i++) {
        for (int j = 0; j < img_width; j++) {

            // -- skip the non-edges --
            if (edge_map_nms(i, j) != EDGE_FINAL_STRONG) 
                continue;

            // -- skip if an edge is already labeled as a contour --
            if (contour_map(i, j) != 0)
                continue;
            
            // -- skip if it is a junction --
            if (junction_endpt_map(i, j) == EDGE_JUNCTION) {
                contour_map(i, j) = -10;
                continue;
            }

            // -- skip if it is not an end-point --
            if (junction_endpt_map(i, j) != EDGE_END)
                continue;

            // -- resets --
            contour_seg_idx = 0;

            // -- mark the edge as a contour point --
            contour_map(i, j) = c_label;

            // -- store in a list where each entry is a contour segment --
            contour_graph_x(contour_node_idx, contour_seg_idx) = i;
            contour_graph_y(contour_node_idx, contour_seg_idx) = j;
            contour_seg_idx++;

            keep_tracking = 1;
            target_x = i;
            target_y = j;

            while (keep_tracking) {
                
                // -- find the next edge to track --
                find_next_edge(target_x, target_y);

                if (junction_endpt_map(next_edge_x, next_edge_y) == EDGE_JUNCTION) {
                    // -- if the next edge to track is a junction, stop teacking --
                    keep_tracking = 0;
                    contour_graph_x(contour_node_idx, contour_seg_idx) = next_edge_x;
                    contour_graph_y(contour_node_idx, contour_seg_idx) = next_edge_y;
                    contour_seg_idx++;
                }
                else if (junction_endpt_map(next_edge_x, next_edge_y) == EDGE_END) {
                    // -- if the next edge to track is an end-point, stop teacking --
                    keep_tracking = 0;
                    contour_map(next_edge_x, next_edge_y) = c_label;
                    contour_graph_x(contour_node_idx, contour_seg_idx) = next_edge_x;
                    contour_graph_y(contour_node_idx, contour_seg_idx) = next_edge_y;
                    contour_seg_idx++;
                }
                else {
                    // -- if the next edge to track is neither a junction nor an end-point, keep teacking --
                    keep_tracking = 1;
                    contour_map(next_edge_x, next_edge_y) = c_label;

                    contour_graph_x(contour_node_idx, contour_seg_idx) = next_edge_x;
                    contour_graph_y(contour_node_idx, contour_seg_idx) = next_edge_y;
                    contour_seg_idx++;

                    target_x = next_edge_x;
                    target_y = next_edge_y;
                }
            }

            //std::cout<<contour_seg_idx<<std::endl;

            c_label++;
            contour_node_idx++;
        }
    }

    write_int_array_to_file("contour_map.txt", contour_map, img_height, img_width);
}

// =========================== track_from_remains ========================
// track edges to form contours, starting from yet to explored edges
// =======================================================================
void EdgeTrackingCPU::track_from_remains() {
    int target_x, target_y;
    bool keep_tracking = 1;
    int contour_seg_idx = 0;

    // -- loop over all images to ensure that the reamining edges are all explored --
    for (int i = 0; i < img_height; i++) {
        for (int j = 0; j < img_width; j++) {
            // -- skip the non-edges --
            if (edge_map_nms(i, j) != EDGE_FINAL_STRONG) 
                continue;
            
            // -- skip if an edge is already labeled as a contour --
            if (contour_map(i, j) != 0)
                continue;

            // -- resets --
            contour_seg_idx = 0;

            // -- mark the edge as a contour point --
            contour_map(i, j) = c_label;

            // -- store in a list where each entry is a contour segment --
            contour_graph_x(contour_node_idx, contour_seg_idx) = i;
            contour_graph_y(contour_node_idx, contour_seg_idx) = j;
            contour_seg_idx++;

            keep_tracking = 1;
            target_x = i;
            target_y = j;

            while (keep_tracking) {
                // -- find the next edge to track --
                find_next_edge_in_loop(target_x, target_y);

                if (next_edge_x == -1 && next_edge_y == -1) {
                    // -- if the tracking process returns to the start, stop tracking --
                    keep_tracking = 0;
                }
                else {
                    // -- if the tracking process does not return to the start, keep tracking tracking --
                    target_x = next_edge_x;
                    target_y = next_edge_y;
                    contour_map(next_edge_x, next_edge_y) = c_label;

                    // -- store in a list where each entry is a contour segment --
                    contour_graph_x(contour_node_idx, contour_seg_idx) = next_edge_x;
                    contour_graph_y(contour_node_idx, contour_seg_idx) = next_edge_y;
                    contour_seg_idx++;

                    keep_tracking = 1;
                }
            }

            c_label++;
            contour_node_idx++;
        }
    }

    write_int_array_to_file("contour_map.txt", contour_map, img_height, img_width);
    write_int_array_to_file("contour_graph_x_list.txt", contour_graph_x, contour_node_idx, img_height*3);
    write_int_array_to_file("contour_graph_y_list.txt", contour_graph_y, contour_node_idx, img_height*3);
}

// ===========================================================================================================================

// =========================== contour_drawing_for_matlab ========================
// output lists for drawing using matlab. Temporarily no need to use this
// ===============================================================================
void EdgeTrackingCPU::contour_drawing_for_matlab() {
    // -- loop over the contour_map to extract subpixel edges and the corresponding contour labels --
    int edge_pt_list_idx = 0;
    for (int i = 0; i < img_height; i++) {
        for (int j = 0; j < img_width; j++) {
            if (edge_map_nms(i, j) == EDGE_FINAL_STRONG) {
                // -- store all necessary information of final edges --
                // -- 1) subpixel location x --
                subpix_contour_list(edge_pt_list_idx, 0) = subpix_pos_x_map(i, j);

                // -- 2) subpixel location y --
                subpix_contour_list(edge_pt_list_idx, 1) = subpix_pos_y_map(i, j);

                // -- 3) contour label --
                subpix_contour_list(edge_pt_list_idx, 2) = contour_map(i, j);

                // -- 4) add up the edge point list index --
                edge_pt_list_idx++;
            }
            else {
                continue;
            }
        }
    }

    // -- write data back so that MATLAB can fetch subpixel data and draw final edge map --
    write_double_array_to_file("data_subpix_contour_output.txt", subpix_contour_list, edge_pt_list_idx, 3);
}

// ==================================== write data to file for debugging =======================================
// Writes data for debugging
// ==============================================================================================================
void EdgeTrackingCPU::write_int_array_to_file(std::string filename, int *wr_data, int first_dim, int second_dim)
{
#define wr_data(i, j) wr_data[(i) * second_dim + (j)]

    std::cout<<"writing data to a file "<<filename<<" ..."<<std::endl;
    std::string out_file_name = "./test_files/";
    out_file_name.append(filename);
	std::ofstream out_file;
    out_file.open(out_file_name);
    if ( !out_file.is_open() )
      std::cout<<"write data file cannot be opened!"<<std::endl;

	for (int i = 0; i < first_dim; i++) {
		for (int j = 0; j < second_dim; j++) {
			out_file << wr_data(i, j) <<"\t";
		}
		out_file << "\n";
	}

    out_file.close();
#undef wr_data
}

void EdgeTrackingCPU::write_double_array_to_file(std::string filename, double *wr_data, int first_dim, int second_dim)
{
#define wr_data(i, j) wr_data[(i) * second_dim + (j)]

    std::cout<<"writing data to a file "<<filename<<" ..."<<std::endl;
    std::string out_file_name = "./test_files/";
    out_file_name.append(filename);
	std::ofstream out_file;
    out_file.open(out_file_name);
    if ( !out_file.is_open() )
      std::cout<<"write data file cannot be opened!"<<std::endl;

	for (int i = 0; i < first_dim; i++) {
		for (int j = 0; j < second_dim; j++) {
			out_file << wr_data(i, j) <<"\t";
		}
		out_file << "\n";
	}

    out_file.close();
#undef wr_data
}


// ==================================== read data from file for debugging =======================================
// Reads data for debugging
// ==============================================================================================================
void EdgeTrackingCPU::read_int_array_from_file(std::string filename, int *rd_data, int first_dim, int second_dim)
{
#define rd_data(i, j) rd_data[(i) * second_dim + (j)]
    std::cout<<"reading data from a file "<<filename<<std::endl;
    std::string in_file_name = "./test_files/";
    in_file_name.append(filename);
    std::fstream in_file;
    int data;
    int idx_x = 0, idx_y = 0;

    in_file.open(in_file_name, std::ios_base::in);
    if (!in_file) {
        std::cerr << "input read file not existed!\n";
    }
    else {
        while (in_file >> data) {
            rd_data(idx_y, idx_x) = data;
            idx_x++;
            if (idx_x == second_dim) {
                idx_x = 0;
                idx_y++;
            }
        }
    }
#undef rd_data
}

void EdgeTrackingCPU::read_double_array_from_file(std::string filename, double *rd_data, int first_dim, int second_dim)
{
#define rd_data(i, j) rd_data[(i) * second_dim + (j)]
    std::cout<<"reading data from a file "<<filename<<std::endl;
    std::string in_file_name = "./test_files/";
    in_file_name.append(filename);
    std::fstream in_file;
    double data;
    int idx_x = 0, idx_y = 0;

    in_file.open(in_file_name, std::ios_base::in);
    if (!in_file) {
        std::cerr << "input read file not existed!\n";
    }
    else {
        while (in_file >> data) {
            rd_data(idx_y, idx_x) = data;
            idx_x++;
            if (idx_x == second_dim) {
                idx_x = 0;
                idx_y++;
            }
        }
    }
#undef rd_data
}

// ===================================== Destructor =======================================
// Free all the 2d dynamic arrays allocated in the constructor
// ========================================================================================
EdgeTrackingCPU::~EdgeTrackingCPU () {
    // free memory
    delete[] edge_map_nms;
    delete[] contour_map;
    delete[] junction_endpt_map;

    delete[] subpix_pos_x_map;
    delete[] subpix_pos_y_map;
    delete[] subpix_contour_list;

    delete[] contour_graph_x;
    delete[] contour_graph_y;
}

#endif    // EdgeTrackingCPU