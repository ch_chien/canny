#ifndef GAUSS_CONV_HPP
#define GAUSS_CONV_HPP

#include <cmath>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string.h>
#include <vector>

#include "indices.hpp"
#include <omp.h>


template<typename T>
class GaussConvCPU {
    int img_height;
    int img_width;
    int kernel_sz;
    T gauss_sigma;

    T *img;
    T *I_conv_x;
    T *I_conv_xy;
    T *I_conv_2d;

  public:

    int omp_threads;

    // timing
    double time_conv_sep, time_conv_2d;

    GaussConvCPU(int, int, int, T, int);
    ~GaussConvCPU();

    void preprocessing(std::ifstream& scan_infile);

    // -- convolve an image with separable Gaussians or 2D Gaussians --
    void convolve_img_separate_Gaussian();
    void convolve_img_2d_Gaussian();

    void write_array_to_file(std::string filename, T *wr_data, int first_dim, int second_dim);
};

// ==================================== Constructor ===================================
// Define parameters used by functions in the class and allocate 2d arrays dynamically
// ====================================================================================
template<typename T>
GaussConvCPU<T>::GaussConvCPU (int H, int W, int kernel_size, T sigma, int nthreads) {
    img_height = H;
    img_width = W;

    kernel_sz = kernel_size;
    gauss_sigma = sigma;

    // openmp threads
    omp_threads = nthreads;

    img            = new T[img_height*img_width];
    I_conv_x       = new T[img_height*img_width];
    I_conv_xy      = new T[img_height*img_width];
    I_conv_2d      = new T[img_height*img_width];
}

// ========================= preprocessing ==========================
// Initialize 2d arrays
// ==================================================================
template<typename T>
void GaussConvCPU<T>::preprocessing(std::ifstream& scan_infile) {

    for (int i = 0; i < img_height; i++) {
        for (int j = 0; j < img_width; j++) {
            img(i, j) = (int)scan_infile.get();
            I_conv_x(i, j) = 0;
            I_conv_xy(i, j) = 0;
            I_conv_2d(i, j) = 0;
        }
    }
}

// ======================= convolve image with seperate Gaussian kernels =====================
// We don't pad the input image here as we assume the padded area are all zeros.
// (1) Start by filling in the x and y mask values of Gaussian functions
// (2) Do a scanning convolution on the input img matrix. This gives us the Δy and Δx matrices
// ===========================================================================================
template<typename T>
void GaussConvCPU<T>::convolve_img_separate_Gaussian()
{
	const int cent = (kernel_sz-1)/2;
	T mask1d[kernel_sz];
	const T PI = 3.14159265358979323846;

	// -- Use the Gaussian function to fill in separate masks --
	for (int p = -cent; p <= cent; p++) {
        // -- Gaussian function in x --
        mask1d[p+cent] = std::exp(-(p*p)/(2*gauss_sigma*gauss_sigma))/(std::sqrt(2*PI)*gauss_sigma);
	}

	// -- do convolution --
    omp_set_num_threads(omp_threads);
    double start = omp_get_wtime();
    #pragma omp parallel for schedule(dynamic)
    // -- do convolution in the x direction --
	for (int i = 0; i < img_height; i++) {
		for (int j = 0; j < img_width; j++) {
			for (int p = -cent; p <= cent; p++) {
                if ((i+p) < 0 || (j+p) < 0 || (i+p) >= img_height || (j+p) >= img_width)
                    continue;

                I_conv_x(i,j) += img(i, j+p) * mask1d[p+cent];
			}
		}
	}
    double first_pass_time = omp_get_wtime() - start;

    start = omp_get_wtime();
    #pragma omp parallel for schedule(dynamic)
    // -- do convolution in the y direction; this time, the convolve img is I_conv_x --
	for (int j = 0; j < img_width; j++) {
        for (int i = 0; i < img_height; i++) {
			for (int p = -cent; p <= cent; p++) {
                if ((i+p) < 0 || (j+p) < 0 || (i+p) >= img_height || (j+p) >= img_width)
                    continue;

                I_conv_xy(i,j) += I_conv_x(i+p, j) * mask1d[p+cent];
			}
		}
	}
    double second_pass_time = omp_get_wtime() - start;
    std::cout<<"- Time of image convolution using separate Gaussians (OpenMP): "<<(first_pass_time+second_pass_time)*1000<<" (ms)"<<std::endl;
    time_conv_sep = (first_pass_time+second_pass_time)*1000;

    //write_array_to_file("separable_conv_output.txt", I_conv_xy, img_height, img_width);
}

template<typename T>
void GaussConvCPU<T>::convolve_img_2d_Gaussian()
{
    const int cent = (kernel_sz-1)/2;
	T mask2d[kernel_sz * kernel_sz];
	const T PI = 3.14159265358979323846;

	// -- Use the Gaussian 1st derivative formula to fill in the mask values --
	for (int p = -cent; p <= cent; p++) {
		for (int q = -cent; q <= cent; q++) {
			// -- 2D Gaussian distribution --
            mask2d(p+cent,q+cent) = std::exp(-(p*p+q*q)/(2*gauss_sigma*gauss_sigma))/(2*PI*gauss_sigma*gauss_sigma);
		}
	}

	// -- do convolution --
    omp_set_num_threads(omp_threads);
    double start = omp_get_wtime();
    #pragma omp parallel for schedule(dynamic)
	for (int i = 0; i < img_height; i++) {
		for (int j = 0; j < img_width; j++) {

			// loop over the kernel
			for (int p = -cent; p <= cent; p++) {
				for (int q = -cent; q <= cent; q++) {
					if ((i+p) < 0 || (j+q) < 0 || (i+p) >= img_height || (j+q) >= img_width)
						continue;

					I_conv_2d(i,j) += img(i+p, j+q) * mask2d(p+cent, q+cent);
				}
			}
		}
	}
    double test_time = omp_get_wtime() - start;
    std::cout<<"- Time of image convolution using a 2D Gaussian (OpenMP): "<<test_time*1000<<" (ms)"<<std::endl;
    time_conv_2d = test_time;

    //write_array_to_file("conv_output_2d.txt", I_conv_2d, img_height, img_width);
}

// ===================================== Write data to file for debugging =======================================
// Writes a 2d dybamically allocated array to a text file for debugging
// ==============================================================================================================
template<typename T>
void GaussConvCPU<T>::write_array_to_file(std::string filename, T *wr_data, int first_dim, int second_dim)
{
#define wr_data(i, j) wr_data[(i) * second_dim + (j)]

    std::cout<<"writing data to a file "<<filename<<" ..."<<std::endl;
    std::string out_file_name = "./test_files/";
    out_file_name.append(filename);
	std::ofstream out_file;
    out_file.open(out_file_name);
    if ( !out_file.is_open() )
      std::cout<<"write data file cannot be opened!"<<std::endl;

	for (int i = 0; i < first_dim; i++) {
		for (int j = 0; j < second_dim; j++) {
			out_file << wr_data(i, j) <<"\t";
		}
		out_file << "\n";
	}

    out_file.close();
#undef wr_data
}

// ===================================== Destructor =======================================
// Free all the 2d dynamic arrays allocated in the constructor
// ========================================================================================
template<typename T>
GaussConvCPU<T>::~GaussConvCPU () {
    // free memory
    delete[] img;
    delete[] I_conv_x;
    delete[] I_conv_xy;
    delete[] I_conv_2d;
}

#endif    // GAUSS_CONV_HPP