#ifndef GPU_CANNY_HPP
#define GPU_CANNY_HPP

#include <cmath>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string.h>
#include <vector>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include "gpu_kernels.hpp"
#include "indices.hpp"

template<typename T>
class CannyGPU {
    int device_id;
    int img_height;
    int img_width;
    int kernel_sz;
    int gauss_sigma;
    T highThr;
    T lowThr;
    int edge_hysteresis_passes;

    T *img, *dev_img;
    T *grad_mag, *dev_grad_mag;
	T *Ix, *Iy, *dev_Ix, *dev_Iy;
    T *dev_subpix_edge_pts_final;
    T *dev_subpix_idx_map;
    T *dev_maskx, *dev_masky;

    T *subpix_pos_x_map, *dev_subpix_pos_x_map;              // -- store x of subpixel location --
    T *subpix_pos_y_map, *dev_subpix_pos_y_map;              // -- store y of subpixel location --
    T *subpix_orientation_map, *dev_subpix_orientation_map;  // -- store orientation of the edge point --
    T *subpix_grad_mag_map, *dev_subpix_grad_mag_map;        // -- store gradient magnitude of the edge points --
    int *dev_edge_label_map;                  // -- store edge point label (0 = no edge, 1 = strong edge, 2 = weak edge) --
    int *dev_edge_count, *dev_strong_candidates, *dev_weak_candidates, *dev_final_edges;

    static const int edge_count_length = 3;
    int edge_count[edge_count_length];


  public:

    int* edge_label_map;
    T* subpix_edge_pts_final;

    int num_of_candidate_strong;
    int num_of_candidate_weak;
    int num_of_total_candidates;
    int edge_pt_list_idx;
    int num_of_edge_data;

    int num_of_final_edge_pts;
    int edge_pt_sz;

    // timing
    float time_conv, time_nms, time_edge_track;
    cudaEvent_t start, stop;

    CannyGPU(int, int, int, int, int, T, int);
    ~CannyGPU();

    void preprocessing(std::ifstream& scan_infile);
    void convolve_img();
    void non_maximum_suppresion();
    void edge_hysteresis();

    void extract_subpix_edges_for_drawing();

    void read_array_from_file(std::string filename, T **rd_data, int first_dim, int second_dim);
    void write_array_to_file(std::string filename, T *wr_data, int first_dim, int second_dim);
};

// ==================================== Constructor ===================================
// Define parameters used by functions in the class and allocate 2d arrays dynamically
// ====================================================================================
template<typename T>
CannyGPU<T>::CannyGPU (int device, int H, int W, int kernel_size, int sigma, T high_thr, int hysteresis_npasses) {
    device_id = device;
    img_height = H;
    img_width = W;
    edge_hysteresis_passes = hysteresis_npasses;

    kernel_sz = kernel_size;
    gauss_sigma = sigma;
    highThr = high_thr;
    lowThr = 0.4 * highThr;

    // cpu
    grad_mag       = new T[img_height*img_width];
    img            = new T[img_height*img_width];
    Ix             = new T[img_height*img_width];
    Iy             = new T[img_height*img_width];

    num_of_edge_data = 4;
    subpix_edge_pts_final   = new T[img_height*img_width*num_of_edge_data];
    subpix_pos_x_map        = new T[img_height*img_width];
    subpix_pos_y_map        = new T[img_height*img_width];
    subpix_orientation_map  = new T[img_height*img_width];
    subpix_grad_mag_map     = new T[img_height*img_width];
    edge_label_map          = new int[img_height*img_width];

    // gpu
    cudacheck( cudaMalloc((void**)&dev_grad_mag, img_height*img_width*sizeof(T)) );
    cudacheck( cudaMalloc((void**)&dev_img,      img_height*img_width*sizeof(T)) );
    cudacheck( cudaMalloc((void**)&dev_Ix,       img_height*img_width*sizeof(T)) );
    cudacheck( cudaMalloc((void**)&dev_Iy,       img_height*img_width*sizeof(T)) );
    cudacheck( cudaMalloc((void**)&dev_maskx,    kernel_sz*kernel_sz *sizeof(T)) );
    cudacheck( cudaMalloc((void**)&dev_masky,    kernel_sz*kernel_sz *sizeof(T)) );

    cudacheck( cudaMalloc((void**)&dev_subpix_edge_pts_final,  img_height*img_width*num_of_edge_data*sizeof(T)) );
    cudacheck( cudaMalloc((void**)&dev_subpix_pos_x_map,       img_height*img_width*sizeof(T)) );
    cudacheck( cudaMalloc((void**)&dev_subpix_pos_y_map,       img_height*img_width*sizeof(T)) );
    cudacheck( cudaMalloc((void**)&dev_subpix_orientation_map, img_height*img_width*sizeof(T)) );
    cudacheck( cudaMalloc((void**)&dev_subpix_grad_mag_map,    img_height*img_width*sizeof(T)) );
    cudacheck( cudaMalloc((void**)&dev_edge_label_map,         img_height*img_width*sizeof(int)   ) );
    cudacheck( cudaMalloc((void**)&dev_edge_count,             edge_count_length*sizeof(int)   ) );

    dev_strong_candidates = dev_edge_count;
    dev_weak_candidates   = dev_strong_candidates + 1;
    dev_final_edges       = dev_weak_candidates + 1;

    time_conv = 0;
    time_nms = 0;
    time_edge_track = 0;

    // cuda event
    cudacheck( cudaEventCreate(&start) );
    cudacheck( cudaEventCreate(&stop) );
}

// ========================= preprocessing ==========================
// Initialize 2d arrays
// ==================================================================
template<typename T>
void CannyGPU<T>::preprocessing(std::ifstream& scan_infile) {

    for (int i = 0; i < img_height; i++) {
        for (int j = 0; j < img_width; j++) {
            img(i, j) = (int)scan_infile.get();
            Ix(i, j) = 0;
            Iy(i, j) = 0;

            subpix_pos_x_map(i, j)       = 0;
            subpix_pos_y_map(i, j)       = 0;
            subpix_orientation_map(i, j) = 0;
            subpix_grad_mag_map(i, j)    = 0;
            edge_label_map(i, j)         = NO_EDGE;
        }
    }

    for (int i = 0; i < img_height*img_width; i++) {
        for (int j = 0; j < num_of_edge_data; j++) {
            subpix_edge_pts_final(i, j)  = 0;
        }
    }

    cudacheck( cudaMemset(dev_Ix,                     0, img_height*img_width*sizeof(T)) );
    cudacheck( cudaMemset(dev_Iy,                     0, img_height*img_width*sizeof(T)) );
    cudacheck( cudaMemset(dev_subpix_pos_x_map,       0, img_height*img_width*sizeof(T)) );
    cudacheck( cudaMemset(dev_subpix_pos_y_map,       0, img_height*img_width*sizeof(T)) );
    cudacheck( cudaMemset(dev_subpix_orientation_map, 0, img_height*img_width*sizeof(T)) );
    cudacheck( cudaMemset(dev_subpix_grad_mag_map,    0, img_height*img_width*sizeof(T)) );
    cudacheck( cudaMemset(dev_edge_count,             0, edge_count_length*sizeof(int   )) );
    cudacheck( cudaMemset(dev_subpix_edge_pts_final,  0, img_height*img_width*num_of_edge_data*sizeof(T)) );

    // because edge_label_map is initialized to -1 (NO_EDGE), copy the buffer to GPU
    // TODO: write an init kernel
    cudacheck( cudaMemcpy(dev_edge_label_map, edge_label_map, img_height*img_width*sizeof(int), cudaMemcpyHostToDevice) );
}

// ================================== convolve Image ==========================================
// We don't pad the input image here as we assume the padded area are all zeros.
// (1) Start by filling in the mask values using the Gausian 1st derivative.
// (2) Do a scanning convolution on the input img matrix. This gives us the Δy and Δx matrices
// (3) Take the sqrt of the sum of Δy^2 and Δx^2 to find the magnitude.
// ============================================================================================
template<typename T>
void CannyGPU<T>::convolve_img()
{
	const int cent = (kernel_sz-1)/2;
	T maskx[kernel_sz * kernel_sz], masky[kernel_sz * kernel_sz];
	T Gx, Gy, G;
	const T PI = 3.14159265358979323846;

	// -- Use the Gaussian 1st derivative formula to fill in the mask values --
	for (int p = -cent; p <= cent; p++) {
		for (int q = -cent; q <= cent; q++) {
			// -- Gaussian derivative in x --
			Gx = (-(q)*std::exp(-(q*q)/(2*gauss_sigma*gauss_sigma)))/(std::sqrt(2*PI)*gauss_sigma*gauss_sigma*gauss_sigma);
			G = std::exp(-(p*p)/(2*gauss_sigma*gauss_sigma))/(std::sqrt(2*PI)*gauss_sigma);
			maskx(p+cent,q+cent) = Gx * G;


			// -- Gaussian derivative in y --
			Gy = (-(p)*std::exp(-(p*p)/(2*gauss_sigma*gauss_sigma)))/(std::sqrt(2*PI)*gauss_sigma*gauss_sigma*gauss_sigma);
			G = std::exp(-(q*q)/(2*gauss_sigma*gauss_sigma))/(std::sqrt(2*PI)*gauss_sigma);
			masky(p+cent,q+cent) = Gy * G;
		}
	}

	cudacheck( cudaMemcpy(dev_maskx, maskx,  kernel_sz*kernel_sz*sizeof(T), cudaMemcpyHostToDevice) );
	cudacheck( cudaMemcpy(dev_masky, masky,  kernel_sz*kernel_sz*sizeof(T), cudaMemcpyHostToDevice) );
	cudacheck( cudaMemcpy(dev_img,   img,   img_height*img_width*sizeof(T), cudaMemcpyHostToDevice) );

	// convolve on gpu
	cudacheck( cudaEventRecord(start) );

	gpu_convolve(device_id, img_height, img_width, dev_img, dev_maskx, dev_masky, dev_Ix, dev_Iy, dev_grad_mag );

	cudacheck( cudaEventRecord(stop) );
	cudacheck( cudaEventSynchronize(stop) );
	cudacheck( cudaEventElapsedTime(&time_conv, start, stop) );
}

// ======================================== Non-maximal Suppression (NMS) ============================================
// (1) Decide the quadrant the gradient belongs to by looking at the signs and size of gradients in x and y directions
// (2) Points which magnitude are greater than both it's neighbors in the direction of their gradients (slope) are
//     considered as peaks.
// (3) Find the subpixel of the edge point by fitting a parabola. This comes from:
//     R. B. Fisher and D. K. Naidu, “A comparison of algorithms for subpixel peak detection,” in Image Technology,
//     Advances in Image Processing, Multimedia and Machine Vis., Berlin, Germany:Springer, 1996, pp. 385–404.
// ====================================================================================================================
template<typename T>
void CannyGPU<T>::non_maximum_suppresion()
{
    T norm_dir_x, norm_dir_y;
    T slope, fp, fm;
    T coeff_A, coeff_B, coeff_C, s, s_star;
    T max_f, subpix_grad_x, subpix_grad_y;
    T candidate_edge_pt_x, candidate_edge_pt_y;
    T subpix_grad_mag;

	cudacheck( cudaEventRecord(start) );

    gpu_nms( device_id, img_height, img_width, highThr, lowThr, dev_Ix, dev_Iy, dev_grad_mag, dev_subpix_pos_x_map,
             dev_subpix_pos_y_map, dev_subpix_grad_mag_map, dev_subpix_orientation_map, dev_edge_label_map,
             dev_strong_candidates, dev_weak_candidates );

	cudacheck( cudaEventRecord(stop) );
	cudacheck( cudaEventSynchronize(stop) );
	cudacheck( cudaEventElapsedTime(&time_nms, start, stop) );
}

// ======================================== Edge Hysteresis (Edge Tracking) ============================================
// Edge hysteresis decides which weak points should be turned into strong points, and which should be disappeared. This
// is done by looking at the 3x3 neighbors of a strong points, and if there is a weak point in the neigbor, make that weak
// point to a strong point. After looking at the neighbors of "ALL the strong points", including those who initially are
// strong points and those who turned from weak to strong, if there is no weak points, then edge hysteresis is finished.
// Process:
// (1) Look at each strong edge in the subpix_edge_pts_strong array; if a weak edge appears in its 3x3 neighborhood, put
//     that weak edge to the end of the subpix_edge_pts_strong array.
// (2) End the process when all edges in the subpix_edge_pts_strong array has been visited.
// ======================================================================================================================
template<typename T>
void CannyGPU<T>::edge_hysteresis()
{

    int final_edge_counter = 0;

    cudacheck( cudaEventRecord(start) );

    gpu_hys(device_id, img_height, img_width, dev_edge_label_map, dev_final_edges, edge_hysteresis_passes );

    cudacheck( cudaEventRecord(stop) );
	cudacheck( cudaEventSynchronize(stop) );
	cudacheck( cudaEventElapsedTime(&time_edge_track, start, stop) );
}

template<typename T>
void CannyGPU<T>::extract_subpix_edges_for_drawing() {

    int local_num_of_strong = 0, local_num_of_weak = 0;
    // retrieve all data from GPU
    cudacheck( cudaMemcpy(Ix,                     dev_Ix,                     img_height*img_width*sizeof(T),   cudaMemcpyDeviceToHost) );
	cudacheck( cudaMemcpy(Iy,                     dev_Iy,                     img_height*img_width*sizeof(T),   cudaMemcpyDeviceToHost) );
	cudacheck( cudaMemcpy(grad_mag,               dev_grad_mag,               img_height*img_width*sizeof(T),   cudaMemcpyDeviceToHost) );
    cudacheck( cudaMemcpy(subpix_pos_x_map,       dev_subpix_pos_x_map,       img_height*img_width*sizeof(T),   cudaMemcpyDeviceToHost) );
    cudacheck( cudaMemcpy(subpix_pos_y_map,       dev_subpix_pos_y_map,       img_height*img_width*sizeof(T),   cudaMemcpyDeviceToHost) );
    cudacheck( cudaMemcpy(subpix_grad_mag_map,    dev_subpix_grad_mag_map,    img_height*img_width*sizeof(T),   cudaMemcpyDeviceToHost) );
    cudacheck( cudaMemcpy(subpix_orientation_map, dev_subpix_orientation_map, img_height*img_width*sizeof(T),   cudaMemcpyDeviceToHost) );
    cudacheck( cudaMemcpy(edge_label_map,         dev_edge_label_map,         img_height*img_width*sizeof(int), cudaMemcpyDeviceToHost) );
    cudacheck( cudaMemcpy(edge_count,             dev_edge_count,             edge_count_length*sizeof(int),    cudaMemcpyDeviceToHost) );

    num_of_final_edge_pts = *edge_count;

    num_of_candidate_strong = edge_count[0];
    num_of_candidate_weak   = edge_count[1];
    num_of_total_candidates = local_num_of_strong + local_num_of_weak;


    // -- loop over to the edge_label_map to extract final edges and extract corresponding subpixels --
    edge_pt_list_idx = 0;
    for (int i = 0; i < img_height; i++) {
        for (int j = 0; j < img_width; j++) {
            if (edge_label_map(i, j) == EDGE_FINAL_STRONG) {
                // -- store all necessary information of final edges --
                // -- 1) subpixel location x --
                subpix_edge_pts_final(edge_pt_list_idx, 0) = subpix_pos_x_map(i, j);

                // -- 2) subpixel location y --
                subpix_edge_pts_final(edge_pt_list_idx, 1) = subpix_pos_y_map(i, j);

                // -- 3) orientation --
                subpix_edge_pts_final(edge_pt_list_idx, 2) = subpix_orientation_map(i, j);

                // -- 4) subpixel gradient magnitude --
                subpix_edge_pts_final(edge_pt_list_idx, 3) = subpix_grad_mag_map(i, j);

                // -- 5) add up the edge point list index --
                edge_pt_list_idx++;
            }
            else {
                continue;
            }
        }
    }
    //std::cout<<"MATLAB fecth data number = "<<edge_pt_list_idx<<std::endl;

    printf(" ## Convolution time = %8.4f ms\n", time_conv );
    printf(" ## NMS         time = %8.4f ms\n", time_nms  );
    printf(" ## Edge-track  time = %8.4f ms\n", time_edge_track );
    printf("\n");
    printf(" Strong candidates   = %6d\n", num_of_candidate_strong);
    printf(" Weak   candidates   = %6d\n", num_of_candidate_weak);
    printf(" Final edge points   = %6d\n", num_of_final_edge_pts);

    // -- write data back so that MATLAB can fetch subpixel data and draw final edge map --
    write_array_to_file("data_final_output_gpu.txt", subpix_edge_pts_final, edge_pt_list_idx, num_of_edge_data);
}


// ===================================== Write data to file for debugging =======================================
// Writes a 2d dybamically allocated array to a text file for debugging
// ==============================================================================================================
template<typename T>
void CannyGPU<T>::write_array_to_file(std::string filename, T *wr_data, int first_dim, int second_dim)
{
#define wr_data(i, j) wr_data[(i) * second_dim + (j)]

    std::cout<<"writing data to a file "<<filename<<" ..."<<std::endl;
    std::string out_file_name = "./test_files/";
    out_file_name.append(filename);
	std::ofstream out_file;
    out_file.open(out_file_name);
    if ( !out_file.is_open() )
      std::cout<<"write data file cannot be opened!"<<std::endl;

	for (int i = 0; i < first_dim; i++) {
		for (int j = 0; j < second_dim; j++) {
			out_file << wr_data(i, j) <<"\t";
		}
		out_file << "\n";
	}

    out_file.close();
#undef wr_data
}

// ===================================== Write data to file for debugging =======================================
// Reads data for debugging
// ==============================================================================================================
template<typename T>
void CannyGPU<T>::read_array_from_file(std::string filename, T **rd_data, int first_dim, int second_dim) {
    std::cout<<"reading data from a file ..."<<std::endl;
    std::string in_file_name = "./test_files/";
    in_file_name.append(filename);
    std::fstream in_file;
    T data;
    int idx_x = 0, idx_y = 0;

    in_file.open(in_file_name, std::ios_base::in);
    if (!in_file) {
        std::cerr << "input read file not existed!\n";
    }
    else {
        while (in_file >> data) {
            rd_data[idx_y][idx_x] = data;
            idx_x++;
            if (idx_x == second_dim) {
                idx_x = 0;
                idx_y++;
            }
        }
    }
}

// ===================================== Destructor =======================================
// Free all the 2d dynamic arrays allocated in the constructor
// ========================================================================================
template<typename T>
CannyGPU<T>::~CannyGPU () {
    // free memory cpu
    delete[] img;
    delete[] grad_mag;
    delete[] Ix;
    delete[] Iy;

    //delete[] subpix_edge_pts_candidates;
    delete[] subpix_edge_pts_final;
    //delete[] subpix_idx_map;

    delete[] subpix_pos_x_map;
    delete[] subpix_pos_y_map;
    delete[] subpix_orientation_map;
    delete[] subpix_grad_mag_map;
    delete[] edge_label_map;

    // free memory gpu
    cudacheck( cudaFree(dev_grad_mag) );
    cudacheck( cudaFree(dev_img) );
    cudacheck( cudaFree(dev_Ix) );
    cudacheck( cudaFree(dev_Iy) );
    cudacheck( cudaFree(dev_maskx) );
    cudacheck( cudaFree(dev_masky) );
    cudacheck( cudaFree(dev_subpix_edge_pts_final) );
    cudacheck( cudaFree(dev_subpix_pos_x_map) );
    cudacheck( cudaFree(dev_subpix_pos_y_map) );
    cudacheck( cudaFree(dev_subpix_orientation_map) );
    cudacheck( cudaFree(dev_subpix_grad_mag_map) );
    cudacheck( cudaFree(dev_edge_label_map) );
    cudacheck( cudaFree(dev_edge_count) );

    cudacheck( cudaEventDestroy(start) );
    cudacheck( cudaEventDestroy(stop) );

}


#endif    // GPU_CANNY_HPP