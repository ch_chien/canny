#include"gpu_kernels.hpp"
#include"indices.hpp"

#define WARP_SIZE (32)
#define NWARPS    ( ((THX*THY) + WARP_SIZE - 1) / WARP_SIZE )
#define ceil_div(a, b) ( ((a) + (b)-1) / (b) )
#define roundup(a, b)  ( ceil_div(a,b) * (b) )

template<int SUBIMGX, int SUBIMGY, int THX, int THY, int PADDING>
__global__
void
gpu_hys_kernel(
        int img_height, int img_width,
        int* dev_edge_label_map,
        int* dev_final_edges )
{
#define sedge_map(i,j) sedge_map[(i) * PIMGX + (j)]
#define tedge_map(i,j) tedge_map[(i) * PIMGX + (j)]

    extern __shared__ int sdata[];

    const int tid = threadIdx.x;
    const int tx  = tid / THX;
    const int ty  = tid % THX;
    const int bx  = blockIdx.x;
    const int by  = blockIdx.y;

    const int PIMGX = SUBIMGX + (2 * PADDING); // padded image x-size
    const int PIMGY = SUBIMGY + (2 * PADDING); // padded image y-size

    const int base_gtx = bx * SUBIMGX + tx;
    const int base_gty = by * SUBIMGY + ty;

    // shared memory ptrs
    int* sedge_map = (int*)sdata;
    int* sflag     = sedge_map + PIMGX * PIMGY;

    int* tedge_map = &sedge_map(PADDING, PADDING);

    // reg. decisions for reading/writing
    bool valid_addr[ceil_div(PIMGY,THY)][ceil_div(PIMGX,THX)];
    bool use_atomics[ceil_div(PIMGY,THY)][ceil_div(PIMGX,THX)];

    // init sedge_map (size PIMGX x PIMGY) to no edges
    for(int itx = tid; itx < (PIMGX*PIMGY); itx += blockDim.x) {
        sedge_map[itx] = NO_EDGE;
    }
    __syncthreads();

    // calculate decisions in registers
    #pragma unroll
    for(int jj = 0; jj < PIMGY; jj+=THY) {
        int gty = base_gty + jj - PADDING;
        #pragma unroll
        for(int ii = 0; ii < PIMGX; ii+=THX) {
            int gtx = base_gtx + ii - PADDING;
            valid_addr[jj/THY][ii/THX] = ( (gtx     >=          0) && (gty     >=         0) &&
                                           (gtx     <  img_height) && (gty     <  img_width) &&
                                           ((tx+ii) <       PIMGX) && ((ty+jj) <      PIMGY)
                                         );

            use_atomics[jj/THY][ii/THX] = ( valid_addr[jj/THY][ii/THX]  &&
                                            ( (tx+ii) < 2  || (tx+ii) > (PIMGX-3) ||
                                              (ty+jj) < 2  || (ty+jj) > (PIMGY-3)    )
                                          );
        }
    }

    // read edge map
    #pragma unroll
    for(int jj = 0; jj < PIMGY; jj+=THY) {
        int gty = base_gty + jj - PADDING;
        #pragma unroll
        for(int ii = 0; ii < PIMGX; ii+=THX) {
            int gtx = base_gtx + ii - PADDING;

            if( valid_addr[jj/THY][ii/THX] ) {
                sedge_map(ii+tx, jj+ty) = dev_edge_label_map(gtx, gty);

            }
        }
    }
    __syncthreads();

    // edge tracking
    int flag = 1, warp_flag = 0;
    int counter = 0;
    while( flag == 1 ) {
        flag = 0;
        #pragma unroll
        for(int jj = 0; jj < SUBIMGY; jj+=THY) {
            int j = jj + ty;
            #pragma unroll
            for(int ii = 0; ii < SUBIMGX; ii+=THX) {
                int i = ii + tx;

                int is_strong = ( tedge_map(i,j) >= EDGE_STRONG ) ? 1 : 0;
                if( is_strong == 1 ) {
                    tedge_map(i,j) = EDGE_FINAL_STRONG;
                }
                //__syncthreads();

                // loop over the 3x3 neighbors
                #pragma unroll
                for (int p = -1; p <= 1; p++) {
                    #pragma unroll
                    for (int q = -1; q <= 1; q++) {
                        int value = tedge_map(i+p, j+q);
                        if (value == EDGE_WEAK && is_strong == 1) {
                            tedge_map(i+p, j+q) = EDGE_STRONG;
                            flag = 1;
                        }
                        //__syncthreads();
                    }
                }
            }
        }

        // reduce flag across threads
        warp_flag = __any_sync(0xFFFFFFFF, flag > 0);
        warp_flag = ( warp_flag == 0 ) ? 0 : 1;
        if( tid % WARP_SIZE == 0 ) {
            sflag[tid / WARP_SIZE] = warp_flag;
        }
        __syncthreads();

        flag = 0;
        #pragma unroll
        for(int iw = 0; iw < NWARPS; iw++) {
            flag = flag | sflag[iw];
        }
        // end of reducing the flag across threads
    }
    __syncthreads();


    // writing
    #pragma unroll
    for(int jj = 0; jj < PIMGY; jj+=THY) {
        int gty = base_gty + jj - PADDING;
        #pragma unroll
        for(int ii = 0; ii < PIMGX; ii+=THX) {
            int gtx = base_gtx + ii - PADDING;

            int value = ( (ii+tx) < PIMGX && (jj+ty) < PIMGY ) ? sedge_map(ii+tx, jj+ty) : NO_EDGE ;
            if( valid_addr[jj/THY][ii/THX] && (value >= EDGE_STRONG) ) {

                if( use_atomics[jj/THY][ii/THY] ) {
                    atomicAdd( &dev_edge_label_map(gtx, gty), value );
                }
                else {
                    dev_edge_label_map(gtx, gty) = value;
                }

            } // valid_addr
        } // loop over PIMGX
    }  // loop over PIMGY

    if(base_gtx < img_height && base_gty < img_width && counter > 0) {
        atomicAdd(dev_final_edges, counter);
    }
}

__global__
void
gpu_hys_aux_kernel(
        int img_height, int img_width,
        int* dev_edge_label_map,
        int* dev_final_edges )
{
    const int tx  = threadIdx.x;
    const int ty  = threadIdx.y;
    const int bx  = blockIdx.x;
    const int by  = blockIdx.y;

    const int gtx = bx * blockDim.x + tx;
    const int gty = by * blockDim.y + ty;

    int value, new_value;
    if( gtx < img_height && gty < img_width ) {
        value = dev_edge_label_map(gtx, gty);
        new_value = ( value >  EDGE_WEAK && value < EDGE_FINAL_STRONG ) ? EDGE_STRONG : value;
        new_value = ( value >= EDGE_STRONG ) ? EDGE_FINAL_STRONG : value;
        if( !(value == new_value) ) {
            dev_edge_label_map(gtx, gty) = new_value;
        }
    }
}

static void
gpu_hys_aux (
        int img_height, int img_width,
        int* dev_edge_label_map, int* dev_final_edges )
{
    const int thread_x = 16;
    const int thread_y = 16;

    const int gridx = (img_height + thread_x - 1) / thread_x;
    const int gridy = (img_width  + thread_y - 1) / thread_y;

    dim3 grid(gridx, gridy, 1);
    dim3 threads(thread_x, thread_y, 1);

    void *kernel_args[] = { &img_height, &img_width, &dev_edge_label_map, &dev_final_edges };
    cudacheck( cudaLaunchKernel((void*)gpu_hys_aux_kernel, grid, threads, kernel_args, 0, NULL) );
}


//extern "C"
void gpu_hys(
        int device_id,
        int h, int w,
        int* dev_edge_label_map,
        int* dev_final_edges,
        int npasses )
{
    // kernel parameters
    // TODO: tune
    const int padding = 1;
    const int subimgx = 16;
    const int subimgy = 16;
    const int thx     = 16;
    const int thy     = 16;
    // end of kernel parameters

    assert(subimgx % thx == 0 && subimgy % thy == 0 && thx == thy);

    const int gridx  = (h + subimgx - 1) / subimgx;
    const int gridy  = (w + subimgy - 1) / subimgy;
    const int nwarps = ( (thx*thy) + WARP_SIZE-1 ) / WARP_SIZE;
    dim3 grid(gridx, gridy, 1);
    dim3 threads(thx*thy,1,1);

    int shmem = 0;
    shmem += sizeof(int) * (subimgx + padding + padding) * (subimgy + padding + padding); // sedge_map
    shmem += sizeof(int) * nwarps;   // sflag

    // get max. dynamic shared memory on the GPU
    int nthreads_max, shmem_max = 0;
    cudacheck( cudaDeviceGetAttribute(&nthreads_max, cudaDevAttrMaxThreadsPerBlock, device_id) );
    #if CUDA_VERSION >= 9000
    cudacheck( cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlockOptin, device_id) );
    if (shmem <= shmem_max) {
        cudacheck( cudaFuncSetAttribute(gpu_hys_kernel<subimgx, subimgy, thx, thy, padding>, cudaFuncAttributeMaxDynamicSharedMemorySize, shmem) );
    }
    #else
    cudacheck( cudaDeviceGetAttribute (&shmem_max, cudaDevAttrMaxSharedMemoryPerBlock, device_id) );
    #endif    // CUDA_VERSION >= 9000

    if ( shmem > shmem_max ) {
        printf("error: %s requires too many threads or too much shared memory\n", __func__);
    }

    void *kernel_args[] = { &h, &w, &dev_edge_label_map, &dev_final_edges };

    for( int ir = 0; ir < npasses; ir++ ) {
        cudacheck( cudaLaunchKernel((void*)gpu_hys_kernel<subimgx, subimgy, thx, thy, padding>, grid, threads, kernel_args, shmem, NULL) );
        // call the correction kernel
        gpu_hys_aux (h, w, dev_edge_label_map, dev_final_edges );
    }
}

