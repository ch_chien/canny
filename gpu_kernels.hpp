#ifndef GPU_KERNELS_HPP
#define GPU_KERNELS_HPP

#include<stdio.h>
#include<assert.h>
#include<cuda.h>
#include<cuda_runtime_api.h>

// cuda error check
#define cudacheck( a )  do { \
                            cudaError_t e = a; \
                            if(e != cudaSuccess) { \
                                printf("\033[1;31m"); \
                                printf("Error in %s:%d %s\n", __func__, __LINE__, cudaGetErrorString(e)); \
                                printf("\033[0m"); \
                            }\
                        } while(0)

//#ifdef __cplusplus
//extern "C" {
//#endif

// single precision convolve
void gpu_convolve(
        int device_id,
        int h, int w, float* dev_img,
        float* dev_maskx, float* dev_masky,
        float* dev_Ix, float* dev_Iy,
        float* dev_grad_mag );


// double precision convolve
void gpu_convolve(
        int device_id,
        int h, int w, double* dev_img,
        double* dev_maskx, double* dev_masky,
        double* dev_Ix, double* dev_Iy,
        double* dev_grad_mag );

void gpu_nms(
        int device_id,
        int h, int w,
        float  highThr, float lowThr,
        float* dev_Ix, float* dev_Iy, float* dev_grad_mag,
        float* dev_subpix_pos_x_map,
        float* dev_subpix_pos_y_map,
        float* dev_subpix_grad_mag_map,
        float* dev_subpix_orientation_map,
        int*    dev_edge_label_map,
        int* dev_strong, int* dev_weak );

// double precision NMS
void gpu_nms(
        int device_id,
        int h, int w,
        double  highThr, double lowThr,
        double* dev_Ix, double* dev_Iy, double* dev_grad_mag,
        double* dev_subpix_pos_x_map,
        double* dev_subpix_pos_y_map,
        double* dev_subpix_grad_mag_map,
        double* dev_subpix_orientation_map,
        int*    dev_edge_label_map,
        int* dev_strong, int* dev_weak );

void gpu_hys(
        int device_id,
        int h, int w,
        int* dev_edge_label_map,
        int* dev_final_edges,
        int npasses );

//#ifdef __cplusplus
//    }
//#endif

#endif // GPU_KERNELS_HPP
