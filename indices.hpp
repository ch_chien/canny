#ifndef INDICES_HPP
#define INDICES_HPP
// macros for flexible axis

#define NO_EDGE             (-1)
#define EDGE_WEAK            (0)
#define EDGE_STRONG          (1)
#define EDGE_FINAL_STRONG    (10)

// cpu
#define img(i,j)                        img[(i) * img_width + (j)]
#define Ix(i,j)                          Ix[(i) * img_width + (j)]
#define Iy(i,j)                          Iy[(i) * img_width + (j)]
#define grad_mag(i,j)              grad_mag[(i) * img_width + (j)]

#define I_conv_x(i,j)              I_conv_x[(i) * img_width + (j)]
#define I_conv_xy(i,j)            I_conv_xy[(i) * img_width + (j)]
#define I_conv_2d(i,j)            I_conv_2d[(i) * img_width + (j)]

#define subpix_pos_x_map(i,j)                 subpix_pos_x_map[(i) * img_width + (j)]
#define subpix_pos_y_map(i,j)                 subpix_pos_y_map[(i) * img_width + (j)]
#define subpix_orientation_map(i,j)     subpix_orientation_map[(i) * img_width + (j)]
#define subpix_grad_mag_map(i,j)           subpix_grad_mag_map[(i) * img_width + (j)]
#define edge_label_map(i,j)                     edge_label_map[(i) * img_width + (j)]

#define dev_subpix_pos_x_map(i,j)              dev_subpix_pos_x_map[(i) * img_width + (j)]
#define dev_subpix_pos_y_map(i,j)              dev_subpix_pos_y_map[(i) * img_width + (j)]
#define dev_subpix_orientation_map(i,j)  dev_subpix_orientation_map[(i) * img_width + (j)]
#define dev_subpix_grad_mag_map(i,j)        dev_subpix_grad_mag_map[(i) * img_width + (j)]
#define dev_edge_label_map(i,j)                  dev_edge_label_map[(i) * img_width + (j)]

#define subpix_edge_pts_final(i,j)       subpix_edge_pts_final[(i) * num_of_edge_data + (j)]

#define maskx(i, j)    maskx[(i) * kernel_sz + (j)]
#define masky(i, j)    masky[(i) * kernel_sz + (j)]
#define mask2d(i, j)  mask2d[(i) * kernel_sz + (j)]

// -- edge tracking / contour extraction --
#define EXTRACT_CONTOURS             (1)
#define edge_map_nms(i, j)           edge_map_nms[(i) * img_width + (j)]
#define contour_map(i, j)            contour_map[(i) * img_width + (j)]
#define junction_endpt_map(i, j)     junction_endpt_map[(i) * img_width + (j)]
#define EDGE_JUNCTION                (1)
#define EDGE_END                     (2)
#define DETECT_JUNCTION_AND_ENDPOINT (1)
#define subpix_contour_list(i, j)    subpix_contour_list[(i) * 3 + (j)]
#define contour_graph_x(i, j)        contour_graph_x[(i) * img_height*3 + (j) ]
#define contour_graph_y(i, j)        contour_graph_y[(i) * img_height*3 + (j) ]

// gpu
#define dev_img(i,j)                        dev_img[(i) * img_width + (j)]
#define dev_Ix(i,j)                          dev_Ix[(i) * img_width + (j)]
#define dev_Iy(i,j)                          dev_Iy[(i) * img_width + (j)]
#define dev_grad_mag(i,j)              dev_grad_mag[(i) * img_width + (j)]
#define dev_subpix_idx_map(i,j)  dev_subpix_idx_map[(i) * img_width + (j)]

#define dev_tmp_weak2strong(i,j)                          dev_tmp_weak2strong[(i) * num_of_edge_data + (j)]
#define dev_subpix_edge_pts_weak(i,j)                dev_subpix_edge_pts_weak[(i) * num_of_edge_data + (j)]
#define dev_subpix_edge_pts_final(i,j)              dev_subpix_edge_pts_final[(i) * num_of_edge_data + (j)]
#define dev_subpix_edge_pts_strong(i,j)            dev_subpix_edge_pts_strong[(i) * num_of_edge_data + (j)]
#define dev_subpix_edge_pts_weak2strong(i,j)  dev_subpix_edge_pts_weak2strong[(i) * num_of_edge_data + (j)]

#define dev_maskx(i, j)    dev_maskx[(i) * kernel_sz + (j)]
#define dev_masky(i, j)    dev_masky[(i) * kernel_sz + (j)]

#endif // INDICES_HPP