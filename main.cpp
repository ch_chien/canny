#include <cmath>
#include <fstream>
#include <iterator>
#include <iostream>
#include <string.h>
#include <vector>
#include <stdint.h>

#include "canny.hpp"
#include "gpu_canny.hpp"
#include "gauss_conv.hpp"

#include "edge_tracking.hpp"

//------------------------------------------------------------------------------
// for error checking
template<typename T>
void check_canny_gpu(
        int img_height, int img_width,
        CannyCPU<T> &ced, CannyGPU<T> &ced_gpu )
{
#define MAX_INCORRECT_EDGES    (50)
#define emap_cpu(i,j)    emap_cpu[(i) * img_width + (j)]
#define emap_gpu(i,j)    emap_gpu[(i) * img_width + (j)]

#define elist_cpu(i,j)   elist_cpu[(i) * edata + (j)]
#define elist_gpu(i,j)   elist_gpu[(i) * edata + (j)]

    // check edge label map
    int* emap_cpu = ced.edge_label_map;
    int* emap_gpu = ced_gpu.edge_label_map;
    int incorrect_edges = 0;
    for (int i = 0; i < img_height; i++) {
        for (int j = 0; j < img_width; j++) {
            if( emap_cpu(i,j) != emap_gpu(i,j) ) {
                incorrect_edges++;
                if( incorrect_edges < MAX_INCORRECT_EDGES ) {
                    printf("Incorrect edge label at (%5d, %5d): cpu = %d, gpu = %d\n", i, j, emap_cpu(i,j), emap_gpu(i,j));
                }
            }
        }
    }

    printf("Total incorrect edges = %d", incorrect_edges);
    if(incorrect_edges > MAX_INCORRECT_EDGES) {
        printf(", but only the first %d are reported\n", MAX_INCORRECT_EDGES);
    }
    else {
        printf("\n");
    }

    T *subpix_edge_pts_final;    // -- a list of final edge points with all information --
    int edge_pt_list_idx;

    int elist_length_cpu = ced.edge_pt_list_idx;
    int elist_length_gpu = ced_gpu.edge_pt_list_idx;

    if( elist_length_cpu != elist_length_gpu ) {
        printf("Subpixel edge lists have different lengths: %d for the CPU, %d for the GPU\n", elist_length_cpu, elist_length_gpu);
        printf("Error checking for the lists entries will be skipped\n");
        return;
    }

    int edata = ced.num_of_edge_data;
    T* elist_cpu = ced.subpix_edge_pts_final;
    T* elist_gpu = ced_gpu.subpix_edge_pts_final;
    T max_err_posx = 0., max_err_posy = 0., max_err_ornt = 0., max_err_grad = 0.;
    for(int i = 0; i <  elist_length_cpu; i++) {
        T err_posx = std::abs(elist_cpu(i,0) - elist_gpu(i,0)) / std::abs(elist_cpu(i,0));
        T err_posy = std::abs(elist_cpu(i,1) - elist_gpu(i,1)) / std::abs(elist_cpu(i,1));
        T err_ornt = std::abs(elist_cpu(i,2) - elist_gpu(i,2)) / std::abs(elist_cpu(i,2));
        T err_grad = std::abs(elist_cpu(i,3) - elist_gpu(i,3)) / std::abs(elist_cpu(i,3));

        max_err_posx = std::max(max_err_posx, err_posx);
        max_err_posy = std::max(max_err_posy, err_posy);
        max_err_ornt = std::max(max_err_ornt, err_ornt);
        max_err_grad = std::max(max_err_grad, err_grad);
    }

    printf("max_err_posx = %8.4e\n", max_err_posx);
    printf("max_err_posy = %8.4e\n", max_err_posy);
    printf("max_err_ornt = %8.4e\n", max_err_ornt);
    printf("max_err_grad = %8.4e\n", max_err_grad);

}

//------------------------------------------------------------------------------
int main(int argc, char **argv)
{
	// -- Exit if the input image file doesn't open --
	std::string filename(argv[1]);
	std::ifstream infile(filename, std::ios::binary);
	if (!infile.is_open())
	{
		std::cout << "File " << filename << " not found in directory." << std::endl;
		return 0;
	}

	char type[10];
	int height, width, intensity;
	// -- Storing header information and copying into the new ouput images --
	infile >> type >> width >> height >> intensity;

	// read number of threads if passed through command line
	int nthreads = 1;
	if(argc > 2) {
	    nthreads = atoi( argv[2] );
	}

	int edge_hysteresis_passes = 6;
	if( argc > 3 ) {
	    edge_hysteresis_passes = atoi( argv[3] );
	}

    int gpu_id = 0;
	if(argc > 4) {
	    gpu_id = atoi( argv[4] );
	}

    #if 0
	cudacheck( cudaSetDevice(gpu_id) );
    #endif

	// -- define parameters --
	// -- (This could be changed to argv input arguments but now let's make it fixed)
	int kernel_size = 17;
	int sigma = 2;
	double high_thr = 8;

    // ==================================== GAUSSIAN CONVOLUTION STARTS HERE ===============================================
    #if 0
    printf("############################################\n");
	printf("##         Double Precision Test          ##\n");
	printf("############################################\n");
    printf("\n ==> CPU Test on Separable Gaussian Kernels and One 2D Gaussian Kernel (%3d OpenMP threads) \n", nthreads);
    printf("==============================================================================================\n");
    GaussConvCPU<double> conv_fp64(height, width, kernel_size, 0.5, nthreads);
    conv_fp64.preprocessing(infile);
    conv_fp64.convolve_img_separate_Gaussian();
    conv_fp64.convolve_img_2d_Gaussian();

    printf("\n\n");
	printf("############################################\n");
	printf("##         Single Precision Test          ##\n");
	printf("############################################\n");

    // go back to the beginning of the file
    infile.clear();
    infile.seekg(0);
    infile >> type >> width >> height >> intensity;

	printf("\n ==> CPU Test on Separable Gaussian Kernels and One 2D Gaussian Kernel (%3d OpenMP threads) \n", nthreads);
    printf("==============================================================================================\n");
	GaussConvCPU<float> conv_fp32(height, width, kernel_size, 0.5, nthreads);
	conv_fp32.preprocessing(infile);
    conv_fp32.convolve_img_separate_Gaussian();
    conv_fp32.convolve_img_2d_Gaussian();
    #endif


    // ==================================== CANNY EDGE DETECTOR STARTS HERE ===============================================
    #if 0
	printf("############################################\n");
	printf("##         Double Precision Test          ##\n");
	printf("############################################\n");

	printf("\n ==> CPU Test (%3d OpenMP threads) \n", nthreads);
	printf("=====================================\n");
	CannyCPU<double> ced_fp64(height, width, kernel_size, sigma, high_thr, nthreads);  // -- class constructor --
	ced_fp64.preprocessing(infile);          // -- preprocessing: array initialization --
	ced_fp64.convolve_img();                 // -- convolve image with Gaussian derivative filter --
	//ced_fp64.non_maximum_suppresion_omp();	// -- non-maximum suppression, openmp version --
    ced_fp64.non_maximum_suppresion();	// -- non-maximum suppression, openmp version --
	ced_fp64.edge_hysteresis_omp();			// -- edge hysteresis (edge tracking), openmp version --

	// -- create a list of final edges with all corresponding information --
	// -- this is only used to give MATLAB to draw final edge map --
	ced_fp64.extract_subpix_edges_for_drawing();

    // go back to the beginning of the file
    infile.clear();
    infile.seekg(0);
    infile >> type >> width >> height >> intensity;

	printf("\n ==> GPU Test \n", nthreads);
	printf("=====================================\n");
	CannyGPU<double> ced_gpu_fp64(gpu_id, height, width, kernel_size, sigma, high_thr, edge_hysteresis_passes);  // -- class constructor --
	ced_gpu_fp64.preprocessing(infile);    // -- preprocessing: array initialization --
	ced_gpu_fp64.convolve_img();           // -- convolve image with Gaussian derivative filter --
    ced_gpu_fp64.non_maximum_suppresion(); // -- do non-maximum suppression --
    ced_gpu_fp64.edge_hysteresis();        // -- do edge hysteresis (edge tracking) --

    // -- create a list of final edges with all corresponding information --
	// -- this is only used to give MATLAB to draw final edge map --
	ced_gpu_fp64.extract_subpix_edges_for_drawing();

	printf("\n ==> Accuracy check \n", nthreads);
	printf("=====================================\n");
	check_canny_gpu<double>(height, width, ced_fp64, ced_gpu_fp64 );

	printf("\n\n");
	printf("############################################\n");
	printf("##         Single Precision Test          ##\n");
	printf("############################################\n");

    // go back to the beginning of the file
    infile.clear();
    infile.seekg(0);
    infile >> type >> width >> height >> intensity;

	printf("\n ==> CPU Test (%3d OpenMP threads) \n", nthreads);
	printf("=====================================\n");
	CannyCPU<float> ced_fp32(height, width, kernel_size, sigma, high_thr, nthreads);  // -- class constructor --
	ced_fp32.preprocessing(infile);          // -- preprocessing: array initialization --
	ced_fp32.convolve_img();                 // -- convolve image with Gaussian derivative filter --
	ced_fp32.non_maximum_suppresion_omp();	// -- non-maximum suppression, openmp version --
	ced_fp32.edge_hysteresis_omp();			// -- edge hysteresis (edge tracking), openmp version --

	// -- create a list of final edges with all corresponding information --
	// -- this is only used to give MATLAB to draw final edge map --
	ced_fp32.extract_subpix_edges_for_drawing();

    // go back to the beginning of the file
    infile.clear();
    infile.seekg(0);
    infile >> type >> width >> height >> intensity;

	printf("\n ==> GPU Test \n", nthreads);
	printf("=====================================\n");
	CannyGPU<float> ced_gpu_fp32(gpu_id, height, width, kernel_size, sigma, high_thr, edge_hysteresis_passes);  // -- class constructor --
	ced_gpu_fp32.preprocessing(infile);    // -- preprocessing: array initialization --
	ced_gpu_fp32.convolve_img();           // -- convolve image with Gaussian derivative filter --
    ced_gpu_fp32.non_maximum_suppresion(); // -- do non-maximum suppression --
    ced_gpu_fp32.edge_hysteresis();        // -- do edge hysteresis (edge tracking) --

    // -- create a list of final edges with all corresponding information --
	// -- this is only used to give MATLAB to draw final edge map --
	ced_gpu_fp32.extract_subpix_edges_for_drawing();

	printf("\n ==> Accuracy check \n", nthreads);
	printf("=====================================\n");
    check_canny_gpu<float>(height, width, ced_fp32, ced_gpu_fp32 );

    // close file
    infile.close();
    #endif

    // ==================================== EDGE TRACKING / CONTOUR EXTRACTION ===============================================
    #if EXTRACT_CONTOURS
    //EdgeTrackingCPU gen_contour(481, 321, 1);  // -- class constructor --
    EdgeTrackingCPU gen_contour(32, 32, 1);  // -- class constructor --
    gen_contour.preprocessing();
    gen_contour.edge_tracking();
    #endif

    return 0;
}
