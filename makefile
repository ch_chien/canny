CUDADIR ?= /gpfs/runtime/opt/cuda/11.1.1/cuda
INC=-I${CUDADIR}/include
LIBDIR=-L${CUDADIR}/lib64
LIB=-lcudart

OPENMP = -fopenmp

NVCCFLAGS  = -O3  -Xcompiler "-fPIC -Wall -Wno-unused-function -Wno-strict-aliasing" -std=c++11
NVCCFLAGS += -gencode arch=compute_70,code=sm_70
NVCCFLAGS += -gencode arch=compute_80,code=sm_80

canny: main.o gpu_convolve.o gpu_nms.o gpu_hys.o
	g++ ${INC} ${OPENMP} -o canny main.o gpu_convolve.o gpu_nms.o gpu_hys.o ${LIBDIR} ${LIB}

main.o: main.cpp
	g++ ${OPENMP} -c main.cpp -O3 -o main.o

#canny.o: canny.cpp
#	g++ -c canny.cpp -O3 ${OPENMP} -o canny.o

#gpu_canny.o: gpu_canny.cpp
#	g++ ${INC} -c gpu_canny.cpp -o gpu_canny.o

gpu_convolve.o: gpu_convolve.cu
	nvcc ${INC} ${NVCCFLAGS} -c gpu_convolve.cu -o gpu_convolve.o

gpu_nms.o: gpu_nms.cu
	nvcc ${INC} ${NVCCFLAGS} -c gpu_nms.cu -o gpu_nms.o

gpu_hys.o: gpu_hys.cu
	nvcc ${INC} ${NVCCFLAGS} -c gpu_hys.cu -o gpu_hys.o

clean:
	rm -f canny *.o
